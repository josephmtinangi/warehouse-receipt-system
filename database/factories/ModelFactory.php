<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Carbon\Carbon;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'full_name' => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'username' => $faker->userName,
        'address' => $faker->address,
        'password' => $password ?: $password = bcrypt('secret'),
        'role_id' => function () {
            return factory('App\Models\Role')->create()->id;
        },
        'status' => $faker->boolean,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Role::class, function (Faker\Generator $faker) {

    return [
        'name' => ucfirst($faker->word),
        'description' => $faker->paragraph,
    ];
});


$factory->define(App\Models\Commodity::class, function (Faker\Generator $faker) {

    return [
        'name' => ucfirst($faker->word),
        'description' => $faker->paragraph,
        'status' => ucfirst($faker->word),
    ];
});

$factory->define(App\Models\PrimaryParty::class, function (Faker\Generator $faker) {

    return [
        'full_business_name' => $faker->company,
        'address' => $faker->address,
        'email' => $faker->companyEmail,
        'phone_number' => $faker->phoneNumber,
        'commodity_id' => function () {
            return factory('App\Models\Commodity')->create()->id;
        },
    ];
});

$factory->define(App\Models\PrimaryPartyGroup::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company,
        'code' => str_random(4),
        'description' => $faker->paragraph,
        'chama_cha_msingi_id' => function () {
            return factory('App\Models\PrimaryParty')->create()->id;
        },
    ];
});

$factory->define(App\Models\Region::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->city,
        'remarks' => $faker->paragraph,
    ];
});

$factory->define(App\Models\District::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->city,
        'remarks' => $faker->paragraph,
    ];
});

$factory->define(App\Models\Location::class, function (Faker\Generator $faker) {
    return [
        'region_id' => function () {
            return factory('App\Models\Region')->create()->id;
        },
        'district_id' => function () {
            return factory('App\Models\District')->create()->id;
        },
    ];
});

$factory->define(App\Models\Bank::class, function (Faker\Generator $faker) {
    return [
        'short_name' => $faker->companySuffix,
        'full_name' => $faker->company,
        'remark' => $faker->paragraph,
    ];
});

$factory->define(App\Models\License::class, function (Faker\Generator $faker) {
    return [
        'number' => str_random(6),
        'type' => $faker->randomDigit,
        'issued_at' => $faker->city,
        'valid_until' => Carbon::tomorrow(),
        'user_id' => function () {
            return factory('App\Models\User')->create()->id;
        },
        'status' => $faker->numberBetween(0, 1),
    ];
});

$factory->define(App\Models\WarehouseOwner::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->freeEmailDomain,
    ];
});

$factory->define(App\Models\WarehouseOperator::class, function (Faker\Generator $faker) {
    return [
        'business_name' => $faker->company,
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->companyEmail,
        'address' => $faker->address,
        'fax' => $faker->phoneNumber,
        'location_id' => function () {
            return factory('App\Models\Location')->create()->id;
        },
        'license_id' => function () {
            return factory('App\Models\License')->create()->id;
        },
        'other' => $faker->sentence,
    ];
});

$factory->define(App\Models\WarehouseOperatorDirector::class, function (Faker\Generator $faker) {
    return [
        'warehouse_operator_id' => function () {
            return factory('App\Models\WarehouseOperator')->create()->id;
        },
        'full_name' => $faker->name,
        'position' => $faker->jobTitle,
        'status' => ucfirst($faker->word),
        'user_id' => function () {
            return factory('App\Models\User')->create()->id;
        },
    ];
});

$factory->define(App\Models\Warehouse::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'warehouse_operator_id' => function () {
            return factory('App\Models\WarehouseOperator')->create()->id;
        },
        'location_id' => function () {
            return factory('App\Models\Location')->create()->id;
        },
        'commodity_id' => function () {
            return factory('App\Models\Commodity')->create()->id;
        },
        'warehouse_owner_id' => function () {
            return factory('App\Models\WarehouseOwner')->create()->id;
        },
        'capacity' => $faker->randomNumber(true),
        'file_number' => $faker->randomNumber(true),
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
        'status' => ucfirst($faker->word),
    ];
});

$factory->define(App\Models\Position::class, function (Faker\Generator $faker) {
    $name = $faker->jobTitle;
    return [
        'name' => str_slug($name),
        'display_name' => $name,
        'description' => $faker->paragraph,
    ];
});

$factory->define(App\Models\MainParty::class, function (Faker\Generator $faker) {
    return [
        'full_business_name' => $faker->company,
        'address' => $faker->address,
        'email' => $faker->companyEmail,
        'phone_number' => $faker->phoneNumber,
        'commodity_id' => function () {
            return factory('App\Models\Commodity')->create()->id;
        },
    ];
});

$factory->define(App\Models\SeasonDetail::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word),
        'description' => $faker->paragraph,
        'start_at' => Carbon::now()->addMonth(1),
        'end_at' => Carbon::now()->addMonth(6),
    ];
});

$factory->define(App\Models\GradeDetail::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word),
        'description' => $faker->paragraph,
    ];
});

