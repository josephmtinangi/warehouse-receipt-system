<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemOwnershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_ownerships', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unsigned()->index();
            $table->enum('status', ['Active', 'Inactive']);
            $table->integer('owner_group_id')->unsigned()->index()->nullable();
            $table->integer('owner_user_id')->unsigned()->index()->nullable();
            $table->enum('owner_flag', ['Individual', 'Group']);
            $table->integer('created_by')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('owner_group_id')->references('id')->on('chama_cha_msingi_group');
            $table->foreign('owner_user_id')->references('id')->on('users');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_ownerships');
    }
}
