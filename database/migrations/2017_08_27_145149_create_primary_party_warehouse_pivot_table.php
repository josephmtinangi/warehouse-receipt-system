<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrimaryPartyWarehousePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('primary_party_warehouse', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('primary_party_id')->unsigned()->index();
            $table->integer('warehouse_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('primary_party_id')->references('id')->on('chama_cha_msingi');
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('primary_party_warehouse');
    }
}
