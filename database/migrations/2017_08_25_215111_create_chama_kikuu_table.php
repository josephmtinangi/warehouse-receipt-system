<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamaKikuuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chama_kikuu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_business_name');
            $table->string('address');
            $table->string('email');
            $table->string('phone_number');
            $table->integer('commodity_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('commodity_id')->references('id')->on('commodities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chama_kikuu');
    }
}
