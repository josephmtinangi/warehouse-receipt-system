<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('warehouse_operator_id')->unsigned()->index();
            $table->integer('location_id')->unsigned()->index();
            $table->integer('commodity_id')->unsigned()->index();
            $table->integer('warehouse_owner_id')->unsigned()->index();
            $table->string('capacity');
            $table->string('file_number');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('warehouse_operator_id')->references('id')->on('warehouse_operators');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('commodity_id')->references('id')->on('commodities');
            $table->foreign('warehouse_owner_id')->references('id')->on('warehouse_owners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
