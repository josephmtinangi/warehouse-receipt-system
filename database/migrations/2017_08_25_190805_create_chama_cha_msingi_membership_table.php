<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamaChaMsingiMembershipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chama_cha_msingi_membership', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chama_cha_msingi_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->string('cheo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chama_cha_msingi_membership');
    }
}
