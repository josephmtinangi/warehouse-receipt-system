<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned()->index();
            $table->integer('item_id')->unsigned()->index();
            $table->double('amount');
            $table->string('issued_at');
            $table->integer('duration')->default(0);
            $table->string('status');
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('item_id')->references('id')->on('items');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
