<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commodity_id')->unsigned()->index();
            $table->string('commodity_origin');
            $table->integer('quantity');
            $table->integer('weight');
            $table->string('grade');
            $table->string('class');
            $table->string('moisture');
            $table->string('property_one');
            $table->string('property_two')->nullable();
            $table->string('property_three')->nullable();
            $table->string('property_four')->nullable();
            $table->integer('group_id')->unsigned()->index()->nullable();
            $table->integer('depositor_user_id')->unsigned()->index();
            $table->integer('depositor_flag')->default(0);
            $table->integer('receiver_user_id')->unsigned()->index();
            $table->integer('warehouse_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('commodity_id')->references('id')->on('commodities');
            $table->foreign('group_id')->references('id')->on('chama_cha_msingi_group');
            $table->foreign('depositor_user_id')->references('id')->on('users');
            $table->foreign('receiver_user_id')->references('id')->on('users');
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
