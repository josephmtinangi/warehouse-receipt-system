<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamaKikuuMembership extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chama_kikuu_membership', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chama_kikuu_id')->unsigned()->index();
            $table->integer('chama_cha_msingi_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('chama_kikuu_id')->references('id')->on('chama_kikuu');
            $table->foreign('chama_cha_msingi_id')->references('id')->on('chama_cha_msingi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chama_kikuu_membership');
    }
}
