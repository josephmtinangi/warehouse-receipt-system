<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->index();
            $table->integer('grade_detail_id')->unsigned()->index();
            $table->integer('season_detail_id')->unsigned()->index();
            $table->string('status');
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
            $table->foreign('grade_detail_id')->references('id')->on('grade_details');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_grades');
    }
}
