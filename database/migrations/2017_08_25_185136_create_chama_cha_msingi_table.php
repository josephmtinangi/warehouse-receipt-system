<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamaChaMsingiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chama_cha_msingi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_business_name');
            $table->string('address');
            $table->string('email')->unique();
            $table->string('phone_number')->unique();
            $table->integer('commodity_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('commodity_id')->references('id')->on('commodities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chama_cha_msingi');
    }
}
