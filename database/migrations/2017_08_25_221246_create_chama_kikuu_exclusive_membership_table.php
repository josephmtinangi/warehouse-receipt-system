<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamaKikuuExclusiveMembershipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chama_kikuu_exclusive_membership', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chama_kikuu_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('cheo')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('chama_kikuu_id')->references('id')->on('chama_kikuu');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chama_kikuu_exclusive_membership');
    }
}
