<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_operators', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name');
            $table->string('phone_number');
            $table->string('email');
            $table->string('address');
            $table->string('fax');
            $table->integer('location_id')->unsigned()->index();
            $table->integer('license_id')->unsigned()->index();
            $table->string('other')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations');
            $table->foreign('license_id')
                  ->references('id')
                  ->on('licenses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_operators');
    }
}
