<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrimaryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('primary_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commodity_id')->unsigned()->index();
            $table->string('quantity');
            $table->string('weight');
            $table->string('grade');
            $table->string('class');
            $table->integer('depositor_user_id')->unsigned()->index();
            $table->integer('receiver_user_id')->unsigned()->index()->nullable();
            $table->foreign('depositor_user_id')->references('id')->on('users');
            $table->foreign('receiver_user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('primary_items');
    }
}
