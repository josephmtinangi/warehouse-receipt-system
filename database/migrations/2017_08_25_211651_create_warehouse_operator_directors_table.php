<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseOperatorDirectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_operator_directors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_operator_id')->unsigned()->index();
            $table->string('full_name');
            $table->string('position');
            $table->string('status');
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('warehouse_operator_id')->references('id')->on('warehouse_operators');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_operator_directors');
    }
}
