<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'Admin']);
        Role::create(['name' => 'Farmer']);
        Role::create(['name' => 'Primary Party']);
        Role::create(['name' => 'Warehouse']);
        Role::create(['name' => 'Main Party']);
        Role::create(['name' => 'Buyer']);
        Role::create(['name' => 'Financial Institution']);
        Role::create(['name' => 'WRRB']);
    }
}
