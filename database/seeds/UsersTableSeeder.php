<?php

use App\Models\MainParty;
use App\Models\Position;
use App\Models\PrimaryParty;
use App\Models\PrimaryPartyGroup;
use App\Models\Role;
use App\Models\User;
use App\Models\WarehouseOperatorDirector;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Position
        $chairperson = App\Models\Position::firstOrCreate(['name' => 'chairperson', 'display_name' => 'Chairperson']);

        // Admin
        $admin = App\Models\User::create(['full_name' => 'Admin','phone_number' => '0719961077','username' => 'admin','password' => bcrypt('admin'),'address' => 'Dodoma','role_id' => App\Models\Role::firstOrCreate(['name' => 'Admin'])->id]);

        // Director
        $director = factory('App\Models\User')->create([
            'username' => 'director',
            'password' => bcrypt('director'),
            'role_id' => Role::firstOrCreate(['name' => 'Warehouse'])->id
        ]);

        $director->director()->save(factory('App\Models\WarehouseOperatorDirector')->create());

        $warehouse = factory('App\Models\Warehouse')->create();

        $director->director->operator->warehouses()->save($warehouse);

        $primaryParty = factory('App\Models\PrimaryParty')->create();

        $warehouse->primaryParties()->attach($primaryParty);

        factory('App\Models\PrimaryPartyGroup')->create(['chama_cha_msingi_id' => $primaryParty->id]);

        $primaryParty->groups()->attach(PrimaryPartyGroup::first());

        // Main Party
        $chama_kikuu = factory('App\Models\User')->create([
            'username' => 'chama_kikuu',
            'password' => bcrypt('chama_kikuu'),
            'role_id' => Role::firstOrCreate(['name' => 'Main Party'])->id
        ]);
        $chama_kikuu->mainParties()->attach(factory('App\Models\MainParty')->create(), ['cheo' => $chairperson->id]);

        // Primary Party
        $chama_cha_msingi = factory('App\Models\User')->create([
            'username' => 'chama_cha_msingi',
            'password' => bcrypt('chama_cha_msingi'),
            'role_id' => Role::firstOrCreate(['name' => 'Primary Party'])->id
        ]);
        $chama_cha_msingi->primaryParties()->attach(factory('App\Models\PrimaryParty')->create(), ['cheo' => $chairperson->id]);

    }
}
