<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ], function () {


    Route::get('/', function () {
        return redirect()->route('login');
    });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    /*
    /-------------
    /	Dashboard
    /-------------
     */

    Route::group(['namespace' => 'Dashboard', 'middleware' => ['auth', 'admin']], function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        Route::resource('dashboard/roles', 'RolesController');
        Route::get('dashboard/roles/{role}/users/create', 'Roles\UsersController@create')->name('roles.users.create');
        Route::post('dashboard/roles/{role}/users', 'Roles\UsersController@store')->name('roles.users.store');

        Route::resource('dashboard/positions', 'PositionsController');
        Route::resource('dashboard/users', 'UsersController');
        Route::resource('dashboard/seasons', 'SeasonsController');
        Route::resource('dashboard/grades', 'GradesController');
        Route::resource('dashboard/regions', 'RegionsController');
        Route::resource('dashboard/districts', 'DistrictsController');
        Route::resource('dashboard/banks', 'BanksController');
        
        Route::resource('dashboard/primary-parties', 'PrimaryPartiesController');
        Route::get('dashboard/primary-parties/{primary_party}/groups', 'PrimaryParties\GroupsController@index')->name('dashboard.primary-parties.groups.index');
        
        Route::resource('dashboard/commodities', 'CommoditiesController');
        Route::resource('dashboard/licenses', 'LicensesController');
        Route::resource('dashboard/location', 'LocationController');
        Route::resource('dashboard/warehouses-owners', 'WarehousesOwnersController');
        Route::resource('dashboard/warehouse-operators', 'WarehouseOperatorsController');
        Route::resource('dashboard/warehouse-operator-directors', 'WarehouseOperatorDirectorsController');

        Route::get('dashboard/warehouses', 'WarehousesController@index')->name('dashboard.warehouses.index');
        Route::get('dashboard/warehouses/create', 'WarehousesController@create')->name('dashboard.warehouses.create');
        Route::post('dashboard/warehouses', 'WarehousesController@store')->name('dashboard.warehouses.store');
        Route::post('dashboard/warehouses/{warehouse}/storegrades', 'WarehousesController@storegrades')->name('dashboard.warehouses.storegrades');
        Route::get('dashboard/warehouses/{warehouse}', 'WarehousesController@show')->name('dashboard.warehouses.show');

        Route::resource('dashboard/main-parties', 'MainPartiesController');

        Route::get('dashboard/main-parties/{main_party}/members/create', 'MainParties\MembersController@create')->name('dashboard.main-parties.main-party.members.create');
        Route::post('dashboard/main-parties/{main_party}/members', 'MainParties\MembersController@store')->name('dashboard.main-parties.main-party.members.store');
    });

// Main Party
    Route::group(['namespace' => 'MainParty', 'middleware' => 'main.party'], function () {
        Route::get('main-parties/{main_party}', 'MainPartyController@show')->name('main-party.main-party.show');
        Route::get('main-parties/{main_party}/primary-parties', 'PrimaryPartyController@index')->name('main-party.main-party.primary-parties.index');
        Route::get('main-parties/{main_party}/primary-parties/create', 'PrimaryPartyController@create')->name('main-party.main-party.primary-parties.create');
        Route::post('main-parties/{main_party}/primary-parties', 'PrimaryPartyController@store')->name('main-party.main-party.primary-parties.store');
        Route::get('main-parties/{main_party}/primary-parties/{primary_party}', 'PrimaryPartyController@show')->name('main-party.main-party.primary-parties.show');

        Route::get('main-parties/{main_party}/members', 'MembersController@index')->name('main-party.main-party.members.index');
        Route::get('main-parties/{main_party}/members/create', 'MembersController@create')->name('main-party.main-party.members.create');
        Route::post('main-parties/{main_party}/members', 'MembersController@store')->name('main-party.main-party.members.store');

        Route::get('main-parties/{main_party}/primary-parties/{primary_party}/members/create', 'PrimaryParties\MembersController@create')->name('main-party.main-party.primary-parties.members.create');
        Route::post('main-parties/{main_party}/primary-parties/{primary_party}/members', 'PrimaryParties\MembersController@store')->name('main-party.main-party.primary-parties.members.store');
    });

// Primary Party
    Route::group(['namespace' => 'PrimaryParty', 'middleware' => ['auth', 'primary.party']], function () {

        Route::get('primary-parties', 'PrimaryPartyController@index')->name('primary-party.primary-party.index');
        Route::get('primary-parties/{primary_party}', 'PrimaryPartyController@show')->name('primary-party.primary-party.show');


        // Primary party members
        Route::get('primary-parties/{primary_party}/members', 'MembersController@index')->name('primary-party.primary-party.members.index');
        Route::get('primary-parties/{primary_party}/members/create', 'PrimaryPartyController@createMember')->name('primary-party.primary-party.members.create');
        Route::get('primary-parties/{primary_party}/members/{member}', 'MembersController@show')->name('primary-party.primary-party.members.show');
        Route::post('primary-parties/{primary_party}/members', 'PrimaryPartyController@storeMember')->name('primary-party.primary-party.members.store');
        Route::post('primary-parties/{primary_party}/members/add-existing', 'PrimaryPartyController@storeExistingMember')->name('primary-party.primary-party.members.storeExisting');

        // Primary party member items
        Route::get('primary-parties/{primary_party}/members/{member}/items', 'Members\ItemsController@index')->name('primary-party.primary-party.members.items.index');
        Route::get('primary-parties/{primary_party}/members/{member}/items/create', 'Members\ItemsController@create')->name('primary-party.primary-party.members.items.create');
        Route::post('primary-parties/{primary_party}/members/{member}/items', 'Members\ItemsController@store')->name('primary-party.primary-party.members.items.store');

        // Primary party group members
        Route::get('primary-parties/{primary_party}/groups/{group}/members', 'Groups\MembersController@index')->name('primary-party.primary-party.groups.members');
        Route::get('primary-parties/{primary_party}/groups/{group}/members/create', 'Groups\MembersController@create')->name('primary-party.primary-party.groups.members.create');
        Route::post('primary-parties/{primary_party}/groups/{group}/members', 'Groups\MembersController@store')->name('primary-party.primary-party.groups.members.store');

        // Primary party groups
        Route::get('primary-parties/{primary_party}/groups', 'GroupsController@index')->name('primary-party.primary-party.groups.index');
        Route::get('primary-parties/{primary_party}/groups/create', 'GroupsController@create')->name('primary-party.primary-party.groups.create');
        Route::get('primary-parties/{primary_party}/groups/{group}', 'GroupsController@show')->name('primary-party.primary-party.groups.show');
        Route::post('primary-parties/{primary_party}/groups', 'GroupsController@store')->name('primary-party.primary-party.groups.store');

        // Primary party group items
        Route::get('primary-parties/{primary_party}/groups/{show}/items', 'Groups\ItemsController@index')->name('primary-party.primary-party.groups.items.index');
        Route::get('primary-parties/{primary_party}/groups/{show}/items/create', 'Groups\ItemsController@create')->name('primary-party.primary-party.groups.items.create');
        Route::post('primary-parties/{primary_party}/groups/{show}/items', 'Groups\ItemsController@store')->name('primary-party.primary-party.groups.items.store');
    });

    //Farmer 
    Route::group(['namespace' => 'Farmer','middleware'=>'auth'],function(){

         
         Route::resource('farmer/primaryItems', 'PrimaryItemsController');
         Route::resource('farmer/secondaryItems', 'SecondaryItemsController');
    });
  

// Warehouse
    Route::group(['namespace' => 'Warehouse', 'middleware' => ['auth', 'director']], function () {
        Route::get('warehouses', 'WarehousesController@index')->name('warehouses.index');
        Route::get('warehouses/{warehouse}', 'WarehousesController@show')->name('warehouses.show');
        Route::get('warehouses/{warehouse}/details', 'WarehousesController@viewdetails')->name('warehouses.viewdetails');

        Route::get('warehouses/{warehouse}/items', 'ItemsController@index')->name('warehouses.items.index');
        Route::post('warehouses/{warehouse}/items', 'ItemsController@store')->name('warehouses.items.store');
        Route::get('warehouses/{warehouse}/items/create', 'ItemsController@create')->name('warehouses.items.create');
        Route::get('warehouses/{warehouse}/items/{item}/receipts/create', 'ItemReceiptsController@create')->name('warehouses.items.receipts.create');
        
        Route::get('warehouses/{warehouse}/items/{item}', 'ItemsController@show')->name('warehouses.items.show');
        
        Route::post('warehouses/{warehouse}/items/{item}/individual-transfer', 'TransferController@individual')->name('warehouses.items.item.individual-transfer');
        Route::post('warehouses/{warehouse}/items/{item}/group-transfer', 'TransferController@group')->name('warehouses.items.item.group-transfer');

        Route::get('warehouses/{warehouse}/farmers', 'FarmersController@index')->name('warehouses.farmers.index');
        Route::post('warehouses/{warehouse}/farmers', 'FarmersController@store')->name('warehouses.farmers.store');
        Route::get('warehouses/{warehouse}/farmers/create', 'FarmersController@create')->name('warehouses.farmers.create');

        Route::get('warehouses/{warehouse}/farmers/{farmer}/items/create', 'Farmers\ItemsController@create')->name('warehouses.farmers.items.create');
        Route::post('warehouses/{warehouse}/farmers/{farmer}/items', 'Farmers\ItemsController@store')->name('warehouses.farmers.items.store');
        Route::get('warehouses/{warehouse}/farmers/{farmer}', 'FarmersController@show')->name('warehouses.farmers.show');

        Route::get('warehouses/{warehouse}/primary-parties', 'PrimaryPartiesController@index')->name('warehouses.primary-parties.index');
        Route::get('warehouses/{warehouse}/primary-parties/{primaryParty}', 'PrimaryPartiesController@show')->name('warehouses.primary-parties.show');

        Route::get('warehouses/{warehouse}/primary-parties/{primaryParty}/groups', 'PrimaryParty\PrimaryPartyGroupsController@index')->name('warehouses.primary-parties.primary-party.groups.index');
        Route::get('warehouses/{warehouse}/primary-parties/{primaryParty}/groups/{group}', 'PrimaryParty\PrimaryPartyGroupsController@show')->name('warehouses.primary-parties.primary-party.groups.show');

        Route::get('warehouses/{warehouse}/primary-parties/{primaryParty}/groups/{group}/items/create', 'PrimaryParty\Groups\ItemsController@create')->name('warehouses.primary-parties.primary-party.groups.items.create');
        Route::post('warehouses/{warehouse}/primary-parties/{primaryParty}/groups/{group}/items', 'PrimaryParty\Groups\ItemsController@store')->name('warehouses.primary-parties.primary-party.groups.items.store');
    });



    Route::middleware('auth')->get('@{username}', function ($username) {
        return \App\Models\User::whereUsername($username)->firstOrFail();
    })->name('user.profile');

});
