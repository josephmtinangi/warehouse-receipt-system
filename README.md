# Warehouse Receipt System

## Users

- Admin
  - username: admin
  - password: admin
  
- Warehouse director
  - username: director
  - password: director
  
- Main Party
  - username: chama_kikuu
  - password: chama_kikuu
  
  
- Primary party
  - username: chama_cha_msingi
  - password: chama_cha_msingi

## Installation

### Server Requirements

- PHP >= 5.6.4

- OpenSSL PHP Extension

- PDO PHP Extension

- Mbstring PHP Extension

- Tokenizer PHP Extension

- XML PHP Extension

### Steps

- Step 1: Clone the repository

  	`git clone url`
  
  
- Step 2: Enter into the project directory and install composer dependencies

  	`composer install`
  
  
- Step 3: Copy `.env.example` to `.env`

  	`cp .env.example .env`
  
  
- Step 4: Change `.env` file according to your credentials

- Step 5: Make sure that directories within the `storage` and the `bootstrap/cache`
  are writable by your web server or this app will not run
  
  