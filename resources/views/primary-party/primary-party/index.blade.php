@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Primary Parties</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    All Primary Parties
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Full business name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Phone number</th>
                            <th>Commodity</th>
                            <th>Position</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i = 1 @endphp
                        @foreach(Auth::user()->primaryParties as $primaryParty)
                            <tr>
                                <td>{{ $i++ }}.</td>
                                <td>
                                    <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">
                                        {{ $primaryParty->full_business_name }}
                                    </a>
                                </td>
                                <td>{{ $primaryParty->address }}</td>
                                <td>{{ $primaryParty->email }}</td>
                                <td>{{ $primaryParty->phone_number }}</td>
                                <td>{{ $primaryParty->commodity->name }}</td>
                                <td>{{ $primaryParty->pivot->cheo }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection
