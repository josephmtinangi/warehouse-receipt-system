@extends('layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $primaryParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-default">
            <div class="panel-heading">About</div>

            <div class="panel-body">

                <dl class="dl-horizontal">
                    <dt>Full business name</dt>
                    <dd>{{ $primaryParty->full_business_name }}</dd>
                    <dt>Address</dt>
                    <dd>{{ $primaryParty->address }}</dd>
                    <dt>Email</dt>
                    <dd>{{ $primaryParty->email }}</dd>
                    <dt>Phone number</dt>
                    <dd>{{ $primaryParty->phone_number }}</dd>
                    <dt>Commodity</dt>
                    <dd>{{ $primaryParty->commodity->name }}</dd>
                    <dt>Members</dt>
                    <dd>{{ $primaryParty->members()->count() }}</dd>
                </dl>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('primary-party.primary-party.members.index', $primaryParty) }}" class="panel-title">
                    Members
                </a>
                ({{ $primaryParty->members()->count() }})
            </div>
            <div class="panel-body">

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('primary-party.primary-party.groups.index', $primaryParty) }}" class="panel-title">
                    Groups
                </a>
                ({{ $primaryParty->groups()->count() }})
            </div>
            <div class="panel-body">



            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="panel-title">
                    Items
                </a>
                (NaN)
            </div>
            <div class="panel-body">
                
            </div>
        </div>

    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
