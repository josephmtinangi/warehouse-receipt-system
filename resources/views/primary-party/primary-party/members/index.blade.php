@extends('layouts.dashboard')

@section('content')


<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">
         <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">{{ $primaryParty->full_business_name }}</a>
    </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $primaryParty->full_business_name }} Members
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Full name</th>
                            <th>Phone number</th>
                            <th>Username</th>
                            <th>Address</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Position</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($primaryParty->members as $user)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('primary-party.primary-party.members.show', [$primaryParty->id, $user->id]) }}">
                                    {{ $user->full_name }}
                                </a>
                            </td>
                            <td>{{ $user->phone_number }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->address }}</td>
                            <td>{{ $user->role->name }}</td>
                            <td>{{ $user->status }}</td>
                            <td>{{ $user->pivot->cheo }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('primary-party.primary-party.members.create', $primaryParty->id) }}" class="btn btn-primary">Register Member</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


@endsection
