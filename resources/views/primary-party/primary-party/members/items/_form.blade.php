<div class="form-group">
    <label for="commodity_id">Commodity</label>
    <select name="commodity_id" id="commodity_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($commodities as $commodity)
            <option value="{{ $commodity->id }}">{{ $commodity->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="quantity">Quantity</label>
    <input type="number" name="quantity" id="quantity" class="form-control" required>
</div>

<div class="form-group">
    <label for="weight">Weight</label>
    <input type="number" name="weight" id="weight" class="form-control" required>
</div>

<div class="form-group">
    <label for="grade">Grade</label>
    <input type="text" name="grade" id="grade" class="form-control" required>
</div>

<div class="form-group">
    <label for="class">Class</label>
    <input type="text" name="class" id="class" class="form-control" required>
</div>
