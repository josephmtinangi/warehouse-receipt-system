@extends('layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
           <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">{{ $primaryParty->full_business_name }}</a>
       </h1>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Item for {{ $member->full_name }}
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                        @include('errors.list')

                        <form action="{{ route('primary-party.primary-party.members.items.store', [$primaryParty->id, $member->id]) }}" method="post">
                            {{ csrf_field() }}

                            @include('primary-party.primary-party.members.items._form')

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>                      

                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@endsection
