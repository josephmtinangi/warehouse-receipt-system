@extends('layouts.dashboard')

@section('content')


<div class="row">
  <div class="col-lg-12">
  <h1 class="page-header">
     <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">{{ $primaryParty->full_business_name }}</a>
  </h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        {{ $member->full_name }}
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">

       <dl class="dl-horizontal">
         <dt>Full name</dt>
         <dd>{{ $member->full_name }}</dd>
         <dt>Phone number</dt>
         <dd>{{ $member->phone_number }}</dd>
         <dt>Username</dt>
         <dd>{{ $member->username }}</dd>
         <dt>Address</dt>
         <dd>{{ $member->address }}</dd>
         <dt>Role</dt>
         <dd>{{ $member->role->name }}</dd>
         <dt>Status</dt>
         <dd>{{ $member->status }}</dd>
         <dt>Last login</dt>
         <dd>{{ $member->last_login }}</dd>
       </dl>

     </div>
     <!-- /.panel-body -->
   </div>
   <!-- /.panel -->

   <div class="panel panel-default">
    <div class="panel-heading">
      <a class="panel-title" href="{{ route('primary-party.primary-party.members.items.index', [$primaryParty->id, $member->id]) }}">
        Items
      </a>
      ({{ $member->items()->count() }})
    </div>
    <div class="panel-body">

    </div>
  </div>

</div>
<!-- /.col-lg-12 -->
</div>


@endsection
