@extends('layouts.dashboard')

@section('content')


<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">
        <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">{{ $primaryParty->full_business_name }}</a>
    </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add member to {{ $primaryParty->full_business_name }}
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                        <h3>Existing</h3>

                      <!-- Display Validation Errors -->
                      @include('errors.list')

                      <form action="{{ route('primary-party.primary-party.members.storeExisting', $primaryParty) }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="user_id">Choose Farmer</label>
                            <select name="user_id" id="user_id" class="form-control" required>
                                <option value="">-Select-</option>
                                @foreach($farmers as $farmer)
                                    <option value="{{ $farmer->id }}">{{ $farmer->full_name }} - {{ $farmer->phone_number }}</option>
                                @endforeach
                            </select>
                        </div>                        

                        <div class="form-group">
                            <label for="cheo">Position</label>
                            <select name="cheo" id="cheo" class="form-control" required>
                                <option value="">-Select-</option>
                                @foreach($positions as $position)
                                <option value="{{ $position->id }}">{{ $position->display_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Register</button>
                    </form>
                    </div>                
                    <div class="col-lg-6">

                        <h3>New</h3>
                      <!-- Display Validation Errors -->
                      @include('errors.list')

                      <form action="{{ route('primary-party.primary-party.members.store', $primaryParty) }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="cheo">Position</label>
                            <select name="cheo" id="cheo" class="form-control" required>
                                <option value="">-Select-</option>
                                @foreach($positions as $position)
                                <option value="{{ $position->id }}">{{ $position->display_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="full_name">Full name</label>
                            <input type="text" name="full_name" id="full_name" value="{{ old('full_name') }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Phone number</label>
                            <input type="tel" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" {{ old('username') }} class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" name="address" id="address" {{ old('address') }} class="form-control" required>
                        </div>                            

                        <button type="submit" class="btn btn-primary">Register</button>
                    </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


@endsection
