@extends('layouts.dashboard')

@section('title', $group->name);

@section('content')

<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">
        <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">{{ $primaryParty->full_business_name }}</a>
    </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               {{ $group->name }}
           </div>

           <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">

                   <dl class="dl-horizontal">
                    <dt>Name</dt>
                    <dd>{{ $group->name }}</dd>
                    <dt>Code</dt>
                    <dd>{{ $group->code }}</dd>
                    <dt>Description</dt>
                    <dd>{{ $group->description }}</dd>
                    <dt>Created</dt>
                    <dd>{{ $group->created_at }}</dd>
                </dl>

            </div>
            <!-- /.col-lg-6 (nested) -->
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->

<div class="panel panel-default">
    <div class="panel-heading">
        <a class="panel-title" href="{{ route('primary-party.primary-party.groups.members', [$primaryParty->id, $group->id]) }}">
            Members
        </a>
        ({{ $group->members()->count() }})
    </div>
    <div class="panel-body">

    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <a class="panel-title" href="{{ route('primary-party.primary-party.groups.items.index', [$primaryParty->id, $group->id]) }}">
            Items
        </a>
        ({{ $group->items()->count() }})
    </div>
    <div class="panel-body">

    </div>
</div>

</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


@endsection
