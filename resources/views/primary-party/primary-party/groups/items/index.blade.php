@extends('layouts.dashboard')

@section('title', 'Group items');

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $group->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            Add Item
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Commodity</th>
                            <th>Quantity</th>
                            <th>Weight</th>
                            <th>Grade</th>
                            <th>Class</th>
                            <th>Depositor</th>
                            <th>Receiver</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($group->items as $item)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $item->commodity->name }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->weight }}</td>
                            <td>{{ $item->grade }}</td>
                            <td>{{ $item->class }}</td>
                            <td>{{ $item->depositor->full_name }}</td>
                            <td>{{ $item->receiver->full_name }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                        @endforeach                      
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('primary-party.primary-party.groups.items.create', [$primaryParty->id, $group->id]) }}" class="btn btn-primary">Add Item</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
