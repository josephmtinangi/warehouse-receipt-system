@extends('layouts.dashboard')

@section('title', 'Add item to ' . $group->name);

@section('content')


<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Add Item to {{ $group->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Item
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                      <form action="{{ route('primary-party.primary-party.groups.items.store', [$primaryParty->id, $group->id]) }}" method="post">
                        {{ csrf_field() }}

                        @include('primary-party.primary-party.members.items._form')


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>


                </div>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


@endsection
