@extends('layouts.dashboard')

@section('title','Groups');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">{{ $primaryParty->full_business_name }}</a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            Groups
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Members</th>
                            <th>Description</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($groups as $group)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('primary-party.primary-party.groups.show', [$primaryParty->id, $group->id]) }}">
                                    {{ $group->name }}
                                </a>
                            </td>
                            <td>{{ $group->code }}</td>
                            <td>{{ $group->members()->count() }}</td>
                            <td>{{ $group->description }}</td>
                            <td>{{ $group->created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('primary-party.primary-party.groups.create', $primaryParty) }}" class="btn btn-primary">Add Group</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
