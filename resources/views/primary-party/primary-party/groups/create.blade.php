@extends('layouts.dashboard')

@section('title','Create group');

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
           <a href="{{ route('primary-party.primary-party.show', $primaryParty->id) }}">{{ $primaryParty->full_business_name }}</a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Create New Group
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                       @include('errors.list')

                       <form action="{{ route('primary-party.primary-party.groups.store', $primaryParty) }}" method="post">
                           {{ csrf_field() }}

                           <div class="form-group">
                               <label for="name">Name</label>
                               <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control" required>
                           </div>

                           <div class="form-group">
                               <label for="description">Description</label>
                               <textarea name="description" id="description" rows="5" class="form-control"></textarea>
                           </div>

                           <button type="submit" class="btn btn-primary">Create</button>
                       </form>

                   </div>
                   <!-- /.col-lg-6 (nested) -->
               </div>
               <!-- /.row (nested) -->
           </div>
           <!-- /.panel-body -->
       </div>
       <!-- /.panel -->
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

   
@endsection
