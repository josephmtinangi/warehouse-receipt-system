@extends('layouts.dashboard')

@section('title', 'Add member');

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $group->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add member
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                     @include('errors.list')

                     <form action="{{ route('primary-party.primary-party.groups.members.store', [$primaryParty->id, $group->id]) }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="user_id">Choose Member</label>
                            <select name="user_id" id="user_id" class="form-control" required>
                                <option value="">-Select-</option>
                                @foreach($members as $user)
                                <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>


                </div>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@endsection
