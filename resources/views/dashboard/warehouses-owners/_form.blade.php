<div class="form-group">
    <label for="name">Full name</label>
    <input type="text" name="name" id="name" class="form-control" required>
</div>
<div class="form-group">
    <label for="address">Address</label>
    <input type="text" name="address" id="address" class="form-control" required>
</div>
<div class="form-group">
    <label for="phone_number">Phone number</label>
    <input type="text" name="phone_number" id="phone_number" rows="5" class="form-control" required>
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="email" name="email" id="email" class="form-control" required>
</div>