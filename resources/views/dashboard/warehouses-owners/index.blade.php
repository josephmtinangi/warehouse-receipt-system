@extends('layouts.dashboard')

@section('title','Warehouses owners');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Warehouses owners</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone number</th>
                            <th>Email</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($warehouses_owners as $warehouses_owner)
                        <tr>
                            <td>{{ $i++}}.</td>
                            <td>{{ $warehouses_owner->name }}</td>
                            <td>{{ $warehouses_owner->address }}</td>
                            <td>{{ $warehouses_owner->phone_number }}</td>
                            <td>{{ $warehouses_owner->email }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                      
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('warehouses-owners.create') }}" class="btn btn-primary">New</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


@endsection
