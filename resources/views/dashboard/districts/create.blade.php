@extends('layouts.dashboard')

@section('title','Create district')

@section('content')
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Create new district</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New District
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('districts.store') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="remarks">Remarks</label>
                                <textarea name="remarks" id="remaks" rows="5" class="form-control"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
