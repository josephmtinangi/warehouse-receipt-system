<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" id="name" class="form-control" required>
</div>
<div class="form-group">
    <label for="display_name">Display name</label>
    <input type="text" name="display_name" id="display_name" class="form-control" required>
</div>
<div class="form-group">
    <label for="discription">Discription</label>
    <textarea name="discription" id="discription" rows="5" class="form-control"></textarea>
</div>