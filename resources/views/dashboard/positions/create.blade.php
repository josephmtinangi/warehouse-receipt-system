@extends('layouts.dashboard')

@section('title','Create positions');

@section('content')

<div class="container-fluid">
   <div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Create positions</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>New position</span>
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('positions.store') }}" method="post">
                            {{ csrf_field() }}

                            @include('dashboard.positions._form')

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
