@extends('layouts.dashboard')

@section('title','Positions');

@section('content')

<div class="container-fluid">
   <div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Positions</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
           <a href="{{ route('positions.create') }}" class="btn btn-primary">New</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Display name</th>
                            <th>Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($positions as $position)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $position->name }}</td>
                            <td>{{ $position->display_name }}</td>
                            <td>{{ $position->description }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div> 
</div>

@endsection
