@extends('layouts.dashboard')

@section('title', 'Create location');

@section('content')
    <div class="container-fluid">
        <div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">New location</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>New Location</span>
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('location.store') }}" method="post">
                            {{ csrf_field() }}

                            @include('dashboard.location._form')

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
