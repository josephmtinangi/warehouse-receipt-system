<div class="form-group">
    <label for="region">Region</label>
    <select name="region" id="region" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($regions as $region)
            <option value="{{ $region->id }}">{{ $region->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="district">District</label>
    <select name="district" id="district" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($districts as $district)
            <option value="{{ $district->id }}">{{ $district->name }}</option>
        @endforeach
    </select>
</div>

    