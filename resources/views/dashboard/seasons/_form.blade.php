<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" id="name" class="form-control" required>
</div>
<div class="form-group">
    <label for="description">Description</label>
    <input type="date" name="description" id="description" class="form-control" required>
</div>
<div class="form-group">
    <label for="start_at">Start at</label>
    <input type="date" name="start_at" id="start_at" class="form-control" required>
</div>
<div class="form-group">
    <label for="end_at">End at</label>
    <input type="date" name="end_at" id="end_at" class="form-control" required>
</div>