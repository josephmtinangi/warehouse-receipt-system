@extends('layouts.dashboard')

@section('title','Create season');

@section('content')
    <div class="container-fluid">
   <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create new season</h1>
    </div>
    </div>
    <!-- /.col-lg-12 -->
        <div class="row">
            <div class="col-lg-12">
                <div class="">
                  

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('seasons.store') }}" method="post">
                            {{ csrf_field() }}

                            @include('dashboard.seasons._form')

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
