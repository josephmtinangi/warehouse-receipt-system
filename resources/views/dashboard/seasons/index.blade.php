@extends('layouts.dashboard')

@section('title','seasons');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Seasons</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
              All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>start_at</th>
                            <th>end_at</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($seasons as $season)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$season->name}}</td>
                            <td>{{$season->description}}</td>
                            <td>{{$season->start_at}}</td>
                            <td>{{ $season->end_at }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('seasons.create') }}" class="btn btn-primary">New</a>
            </div>            
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
