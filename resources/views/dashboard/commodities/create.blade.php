@extends('layouts.dashboard')

@section('title','Create commodity');

@section('content')
    <div class="container-fluid">
        <div class="row">
        <div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Create new commodity</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New commodity</h3>
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('commodities.store') }}" method="post">
                            {{ csrf_field() }}

                            @include('dashboard.commodities._form')

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
