@extends('layouts.dashboard')

@section('title','Commodities');

@section('content')


<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Commodities</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                All Commodities
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($commodities as $commodity)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $commodity->name }}</td>
                            <td>{{ $commodity->description }}</td>
                            <td>{{ $commodity->status }}</td>
                            <td>{{ $commodity->created_at }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                      
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('commodities.create') }}" class="btn btn-primary">New</a>
            </div>            
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
