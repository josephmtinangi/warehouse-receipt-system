@foreach($users as $user)
    <tr>
        <td>{{ $i++ }}.</td>
        <td>{{ $user->full_name }}</td>
        <td>{{ $user->phone_number }}</td>
        <td>{{ $user->username }}</td>
        <td>{{ $user->address }}</td>
        <td>{{ $user->role->name }}</td>
        <td>{{ $user->status }}</td>
        <td>{{ $user->created_at }}</td>
        <td>{{ $user->last_login }}</td>
    </tr>
@endforeach