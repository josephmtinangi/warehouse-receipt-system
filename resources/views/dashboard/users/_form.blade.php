<div class="form-group">
    <label for="full_name">Full name</label>
    <input type="text" name="full_name" id="full_name" class="form-control" required>
</div>
<div class="form-group">
    <label for="phone_number">Phone number</label>
    <input type="text" name="phone_number" id="phone_number" class="form-control" required>
</div>
<div class="form-group">
    <label for="username">Username</label>
    <input type="text" name="username" id="username" class="form-control" required>
</div>
<div class="form-group">
    <label for="address">Address</label>
    <input type="text" name="address" id="address" class="form-control" required>
</div>
<div class="form-group">
    <label for="role_id">Select role</label>
    <select name="role_id" id="role_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($roles as $role)
            <option value="{{ $role->id }}">{{ $role->name }}</option>
        @endforeach
    </select>
</div>
  