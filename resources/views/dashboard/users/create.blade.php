@extends('layouts.dashboard')

@section('title','create users')

@section('content')
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create new user</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New user</h3>
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('users.store') }}" method="post">
                            {{ csrf_field() }}
                            @include('dashboard.users._form')
                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
