@extends('layouts.dashboard')

@section('title','Users')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Users</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
              All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Full name</th>
                            <th>Phone number</th>
                            <th>Username</th>
                            <th>Address</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Last login</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @include('dashboard.users.list')
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('users.create') }}" class="btn btn-primary">New</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


@endsection
