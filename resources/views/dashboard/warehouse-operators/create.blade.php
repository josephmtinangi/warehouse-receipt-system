@extends('layouts.dashboard')

@section('title','Create operator');

@section('content')
    <div class="container-fluid">
        <div class="row">
         
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create warehouse operators</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>New warehouse operator</span>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ route('warehouse-operators.store') }}" method="post">
                            {{ csrf_field() }}
                            @include('dashboard.warehouse-operators._form')
                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
