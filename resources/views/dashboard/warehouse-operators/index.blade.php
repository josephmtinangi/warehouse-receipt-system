@extends('layouts.dashboard')

@section('title','Warehouse operators');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Warehouse Operators</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Business name</th>
                            <th>Phone number</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>fax</th>
                            <th>Location</th>
                            <th>License No</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($warehouse_operators as $warehouse_operator)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $warehouse_operator->business_name }}</td>
                            <td>{{ $warehouse_operator->phone_number }}</td>
                            <td>{{ $warehouse_operator->email }}</td>
                            <td>{{ $warehouse_operator->address }}</td>
                            <td>{{ $warehouse_operator->fax }}</td>
                            <td>{{ $warehouse_operator->location_id }}</td>
                            <td>{{ $warehouse_operator->license_id }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                      
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('warehouse-operators.create') }}" class="btn btn-primary">New</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


@endsection
