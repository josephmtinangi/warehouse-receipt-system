<div class="form-group">
    <label for="business_name">Business name</label>
    <input type="text" name="business_name" id="business_name" class="form-control" required>
</div>
<div class="form-group">
    <label for="phone_number">Phone number</label>
    <input type="tel" name="phone_number" id="phone_number" class="form-control" required>
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="email" name="email" id="email" class="form-control" required>
</div>
<div class="form-group">
    <label for="address">Address</label>
    <input type="text" name="address" id="address" class="form-control" required>
</div>
<div class="form-group">
    <label for="fax">Fax</label>
    <input type="text" name="fax" id="fax" class="form-control" required>
</div>
<div class="form-group">
    <label for="location_id">Location</label>
    <select name="location_id" id="location_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($locations as $location)
            <option value="{{ $location->id }}">{{ $location->region->name }}, {{ $location->district->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="license_id">Licence</label>
    <select name="license_id" id="license_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($licenses as $license)
            <option value="{{ $license->id }}">{{ $license->number }}</option>
        @endforeach
    </select>
</div>
    
  