@extends('layouts.dashboard')

@section('title','Warehouse Operator Directors');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Warehouse Operator Directors</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                 All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Phone number</th>
                            <th>Operator</th>
                            <th>Position</th>
                            <th>created_at</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($directors as $director)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $director->full_name }}</td>
                            <td>{{ $director->user->username }}</td>
                            <td>{{ $director->user->phone_number }}</td>
                            <td>{{ $director->operator->business_name }}</td>
                            <td>{{ $director->position }}</td>
                            <td>{{ $director->created_at }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                      
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('warehouse-operator-directors.create') }}" class="btn btn-primary">New</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
