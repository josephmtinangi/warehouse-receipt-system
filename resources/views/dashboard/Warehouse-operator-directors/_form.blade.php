<div class="form-group">
    <label for="warehouse_operator_id">Warehouse Operator</label>
    <select name="warehouse_operator_id" id="warehouse_operator_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($warehouse_operators as $warehouse_operator)
            <option value="{{ $warehouse_operator->id }}">{{ $warehouse_operator->business_name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="full_name">Full name</label>
    <input type="text" name="full_name" id="full_name" class="form-control" required>
</div>
<div class="form-group">
    <label for="phone_number">Phone number</label>
    <input type="text" name="phone_number" id="phone_number" class="form-control" required>
</div>
<div class="form-group">
    <label for="username">Username</label>
    <input type="text" name="username" id="username" class="form-control" required>
</div>
<div class="form-group">
    <label for="address">Address</label>
    <input type="text" name="address" id="address" class="form-control" required>
</div>
<div class="form-group">
    <label for="position">Position</label>
    <input type="text" name="position" id="position" class="form-control" required>
</div>
<div class="form-group">
    <label for="status">Status</label>
    <input type="text" name="status" id="sta" class="form-control">
</div>
  