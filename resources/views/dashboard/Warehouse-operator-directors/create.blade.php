@extends('layouts.dashboard')

@section('title','Create director');

@section('content')
    <div class="container-fluid">
        <div class="row">
         
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create directors</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>New Operator director</span>
                    </div>

                    <div class="panel-body">

                        <form action="{{ route('warehouse-operator-directors.store') }}" method="post">
                            {{ csrf_field() }}

                            @include('dashboard.warehouse-operator-directors._form')

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
