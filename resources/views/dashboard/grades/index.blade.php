@extends('layouts.dashboard')

@section('title','grades');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">grades</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
              All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>created_at</th>
                            <th>updated_at</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($grades as $grade)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$grade->name}}</td>
                            <td>{{$grade->description}}</td>
                            <td>{{$grade->created_at}}</td>
                            <td>{{ $grade->updated_at }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('grades.create') }}" class="btn btn-primary">New</a>
            </div>            
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
