@extends('layouts.dashboard')

@section('title','Create grade');

@section('content')
    <div class="container-fluid">
   <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create new grade</h1>
    </div>
    </div>
    <!-- /.col-lg-12 -->
        <div class="row">
            <div class="col-lg-12">
                <div class="">
                  

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('grades.store') }}" method="post">
                            {{ csrf_field() }}

                            @include('dashboard.grades._form')

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
