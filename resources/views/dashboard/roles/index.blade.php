@extends('layouts.dashboard')

@section('title', 'Roles')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Roles</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                All Roles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>User Count</th>
                            <th>Created</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i = 1 @endphp
                    @foreach($roles as $role)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('roles.show', $role->id) }}">
                                    {{ $role->name }}
                                </a>
                            </td>
                            <td>{{ $role->description }}</td>
                            <td><span class="label label-primary">{{ $role->users_count }}</span></td>
                            <td>{{ $role->created_at->toDateString() }}</td>
                            <td>
                                <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                        </tr>
                    @endforeach                        
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                {{-- <a href="{{ route('roles.create') }}" class="btn btn-primary">New</a> --}}
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
