@extends('layouts.dashboard')

@section('title', 'Assign ' . $role->name . ' to user')

@section('content')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Assign {{ $role->name }} to user</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Basic Form Elements
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">

                       <form action="{{ route('roles.users.store', $role->id) }}" method="post" role="form">
                           {{ csrf_field() }}

                           <div class="form-group">
                               <label for="user_id">Select User</label>
                               <select name="user_id" id="user_id" class="form-control" required>
                                   <option value="">-Select-</option>
                                   @foreach($users as $user)
                                   <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                                   @endforeach
                               </select>
                           </div>

                           <button type="submit" class="btn btn-primary">Assign</button>

                       </form>

                   </div>
                   <!-- /.col-lg-6 (nested) -->
               </div>
               <!-- /.row (nested) -->
           </div>
           <!-- /.panel-body -->
       </div>
       <!-- /.panel -->
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@endsection
