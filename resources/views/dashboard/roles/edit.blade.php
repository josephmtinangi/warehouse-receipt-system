@extends('layouts.dashboard')

@section('title', 'Edit ' . $role->name)

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit {{ $role->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div clasD="panel-heading">
                Edit {{ $role->name }}
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <form action="{{ route('roles.update', $role->id) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" value="{{ $role->name or old('name') }}" class="form-control" disabled>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" rows="5" class="form-control">{{ $role->description or old('description') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                {{-- <a href="{{ route('roles.create') }}" class="btn btn-primary">New</a> --}}
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
