@extends('layouts.dashboard')

@section('title', $role->name)

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $role->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DataTables Advanced Tables
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Username</th>
                            <th>Full name</th>
                            <th>Phone number</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @php $i = 1 @endphp
                        @foreach($role->users as $user)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->full_name }}</td>
                            <td>{{ $user->phone_number }}</td>
                            <td>{{ $user->address }}</td>
                            <td>
                                <a href="#" class="btn btn-danger btn-sm">Revoke</a>
                            </td>
                        </tr>
                        @endforeach                        
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('roles.users.create', $role->id) }}" class="btn btn-primary">Add User</a>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
