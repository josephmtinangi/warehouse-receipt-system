@extends('layouts.dashboard')

@section('title','Banks');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Banks</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
              All Banks
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Short name</th>
                            <th>Full name</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($banks as $bank)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $bank->short_name }}</td>
                            <td>{{ $bank->full_name }}</td>
                            <td>{{ $bank->created_at->toFormattedDateString() }}</td>
                            <td>{{ $bank->updated_at->toFormattedDateString() }}</td>
                            <td>
                                <a href="{{ route('banks.edit', $bank->id) }}" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                            <td>
                                <a class="btn btn-danger" data-toggle="modal" href='#delete-bank-{{ $bank->id }}'><i class="fa fa-trash"></i> Delete</a>
                                <div class="modal fade" id="delete-bank-{{ $bank->id }}">
                                    <div class="modal-dialog">
                                        <form action="{{ route('banks.destroy', $bank->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Deleting {{ $bank->full_name }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Delete <strong>{{ $bank->full_name }}</strong>?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                                </div>
                                            </div>
                                                                                    
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('banks.create') }}" class="btn btn-primary">New</a>
            </div>            
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
