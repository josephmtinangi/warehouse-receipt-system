<div class="form-group">
    <label for="full_name">Full name</label>
    <input type="text" name="full_name" id="full_name" value="{{ $bank->full_name or old('full_name') }}" class="form-control" required>
</div>
<div class="form-group">
    <label for="short_name">Short name</label>
    <input type="text" name="short_name" id="short_name" value="{{ $bank->short_name or old('short_name') }}" class="form-control" required>
</div>
<div class="form-group">
    <label for="remarks">Remarks</label>
    <textarea name="remarks" id="remaks" rows="5" class="form-control">{{ $bank->remarks or old('remarks') }}</textarea>
</div>