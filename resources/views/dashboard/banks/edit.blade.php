@extends('layouts.dashboard')

@section('title', 'Edit ' . $bank->full_name);

@section('content')
    <div class="container-fluid">
   <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit {{ $bank->full_name }}</h1>
    </div>
    </div>
    <!-- /.col-lg-12 -->
        <div class="row">
            <div class="col-lg-12">
                <div class="">
                  

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('banks.update', $bank->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            @include('dashboard.banks._form')

                            <button type="submit" class="btn btn-primary">Update</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
