<div class="form-group">
    <label for="number">Number</label>
    <input type="text" name="number" id="number" class="form-control" required>
</div>
<div class="form-group">
    <label for="type">Type</label>
    <input type="number" name="type" id="type" class="form-control" required>
</div>
<div class="form-group">
    <label for="issued_at">Issued at</label>
    <input type="text" name="issued_at" id="issued_at" rows="5" class="form-control" required>
</div>
<div class="form-group">
    <label for="valid_until">Valid until</label>
    <input type="date" name="valid_until" id="valid_until" rows="5" class="form-control" required>
</div>
    