@extends('layouts.dashboard')

@section('title','Licenses');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Licenses</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                All Licenses
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Number</th>
                            <th>Type</th>
                            <th>Issued</th>
                            <th>Valid Untill</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($licenses as $license)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $license->number }}</td>
                            <td>{{ $license->type }}</td>
                            <td>{{ $license->issued_at }}</td>
                            <td>{{ $license->valid_until }}</td>
                            <td>{{ $license->created_at }}</td>
                            <td>{{ $license->updated_at }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                     
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('licenses.create') }}" class="btn btn-primary">New</a>
            </div>
        </div>            
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
