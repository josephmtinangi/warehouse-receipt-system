@extends('layouts.dashboard')

@section('title','Create primary parties');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create primary parties</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New Primary Party</h3>
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('primary-parties.store') }}" method="post">
                            {{ csrf_field() }}

                            @include('dashboard.primary-parties._form')

                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
