@extends('layouts.dashboard')

@section('title', $primaryParty->full_business_name . ' Groups');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <a href="{{ route('primary-parties.show', $primaryParty->id) }}">
                {{ $primaryParty->full_business_name }}
            </a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
       
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="panel-title" href="{{ route('dashboard.primary-parties.groups.index', $primaryParty->id) }}">
                    Groups
                </a>
                 ({{ $primaryParty->groups()->count() }})
            </div>
            <div class="panel-body">

                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Members</th>
                            <th>Registered</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($primaryParty->groups as $group)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $group->name }}</td>
                            <td>{{ $group->code }}</td>
                            <td>{{ $group->members()->count() }}</td>
                            <td>{{ $group->created_at }}</td>
                        </tr>
                        @endforeach                        
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
        </div>

    </div>
    <!-- /.col-lg-12 -->
</div>


@endsection
