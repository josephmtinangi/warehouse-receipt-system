@extends('layouts.dashboard')

@section('title','Primary parties');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Primary parties</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Phone number</th>
                            <th>Commodity</th>
                            <th>Created</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($primaryParties as $primaryParty)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('primary-parties.show', $primaryParty->id) }}">
                                    {{ $primaryParty->full_business_name }}
                                </a>
                            </td>
                            <td>{{ $primaryParty->address }}</td>
                            <td>{{ $primaryParty->email }}</td>
                            <td>{{ $primaryParty->phone_number }}</td>
                            <td>{{ $primaryParty->commodity->name }}</td>
                            <td>{{ $primaryParty->created_at }}</td>
                            <td><a href="{{ route('primary-parties.edit', $primaryParty->id) }}" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('primary-parties.create') }}" class="btn btn-primary">New</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


@endsection
