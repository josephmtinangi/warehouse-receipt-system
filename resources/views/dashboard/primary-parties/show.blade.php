@extends('layouts.dashboard')

@section('title', $primaryParty->full_business_name)

@section('content')
<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">{{ $primaryParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $primaryParty->full_business_name }}
            </div>

            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>Full business name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Phone number</th>
                                <th>Commodity</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $primaryParty->full_business_name }}</td>
                                <td>{{ $primaryParty->address }}</td>
                                <td>{{ $primaryParty->email }}</td>
                                <td>{{ $primaryParty->phone_number }}</td>
                                <td>{{ $primaryParty->commodity->name }}</td>
                                <td>{{ $primaryParty->created_at }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Members</h3>
            </div>
            <div class="panel-body">

                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Full Name</th>
                            <th>Username</th>
                            <th>Phone number</th>
                            <th>Position</th>
                            <th>Registered</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($primaryParty->members as $member)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $member->full_name }}</td>
                            <td>{{ $member->username }}</td>
                            <td>{{ $member->phone_number }}</td>
                            <td>
                                @foreach($positions as $position)
                                    @if($member->pivot->cheo == $position->id)
                                        {{ $position->display_name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ $member->created_at }}</td>
                        </tr>
                        @endforeach                        
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="panel-title" href="{{ route('dashboard.primary-parties.groups.index', $primaryParty->id) }}">
                    Groups
                </a>
                 ({{ $primaryParty->groups()->count() }})
            </div>
            <div class="panel-body">

               

            </div>
        </div>
    </div>
</div>
@endsection
