@extends('layouts.dashboard')

@section('title','Edit ' . $primaryParty->full_business_name);

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit {{ $primaryParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit {{ $primaryParty->full_business_name }}</h3>
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('primary-parties.update', $primaryParty->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            @include('dashboard.primary-parties._form')

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
