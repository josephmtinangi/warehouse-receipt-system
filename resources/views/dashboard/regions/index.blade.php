@extends('layouts.dashboard')

@section('title','Regions')

@section('content') 

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Regions</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               <a href="{{ route('regions.create') }}" class="btn btn-primary">New</a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Remarks</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($regions as $region)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $region->name }}</td>
                            <td>{{ $region->remarks }}</td>
                            <td>{{ $region->created_at }}</td>
                            <td>{{ $region->updated_at }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
