@extends('layouts.dashboard')

@section('title','Create region')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Create new region</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                New Region
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">


                        @include('errors.list')

                        <form action="{{ route('regions.store') }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="remarks">Remarks</label>
                                <textarea name="remarks" id="remaks" rows="5" class="form-control"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form> 

                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@endsection
