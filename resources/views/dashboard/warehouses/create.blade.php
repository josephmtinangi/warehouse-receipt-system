@extends('layouts.dashboard')

@section('title','Create warehouse');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Warehouse operators</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        New Warehouse
                    </div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ route('dashboard.warehouses.store') }}" method="post">
                            {{ csrf_field() }}
                            @include('dashboard.warehouses._form')
                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
