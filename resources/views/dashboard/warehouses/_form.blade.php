<div class="form-group">
    <label for="name">Name</label>
    <input type="text" name="name" id="name" class="form-control" required>
</div>
<div class="form-group">
    <label for="operator">Operator</label>
    <select name="operator" id="operator" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($warehouse_operators as $warehouse_operator)
            <option value="{{$warehouse_operator->id}}">{{ $warehouse_operator->business_name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="location">Location</label>
    <select name="location" id="location" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($locations as $location)
            <option value="{{ $location->id }}">{{ $location->region->name }}, {{ $location->district->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="commodity_id">Commodity</label>
    <select name="commodity_id" id="commodity_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($commodities as $commodity)
            <option value="{{ $commodity->id }}">{{ $commodity->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="warehouse_owner_id">Owner</label>
    <select name="warehouse_owner_id" id="warehouse_owner_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($warehouses_owners as $owner)
            <option value="{{ $owner->id }}">{{ $owner->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="file_number">File number</label>
    <input type="text" name="file_number" id="file_number" class="form-control">
</div>
<div class="form-group">
    <label for="capacity">Capacity</label>
    <input type="text" name="capacity" id="capacity" class="form-control">
</div>
<div class="form-group">
    <label for="longitude">Longitude</label>
    <input type="number" name="longitude" id="longitude" class="form-control">
</div>
<div class="form-group">
    <label for="latitude">Latitude</label>
    <input type="number" name="latitude" id="latitude" class="form-control">
</div>
<div class="form-group">
    <label for="status">Status</label>
    <input type="text" name="status" id="status" class="form-control">
</div>