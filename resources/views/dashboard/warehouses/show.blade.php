@extends('layouts.dashboard')

@section('title', $warehouse->name);

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $warehouse->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $warehouse->name }}
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td>{{ $warehouse->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Operator</th>
                                        <td>{{ $warehouse->operator->business_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Owner</th>
                                        <td>{{ $warehouse->owner->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Commodity</th>
                                        <td>{{ $warehouse->commodity->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Capacity</th>
                                        <td>{{ $warehouse->capacity }}</td>
                                    </tr>
                                    <tr>
                                        <th>Region</th>
                                        <td>{{ $warehouse->location->region->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>District</th>
                                        <td>{{ $warehouse->location->district->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Lat, Lng</th>
                                        <td>({{ $warehouse->latitude }}, {{ $warehouse->longitude }})</td>
                                    </tr>
                                    <tr>
                                        <th>File number</th>
                                        <td>{{ $warehouse->file_number }}</td>
                                    </tr>  
                                    <tr>
                                        <th>Status</th>
                                        <td>{{ $warehouse->status }}</td>
                                    </tr> 
                                    <tr>
                                        <th>Registered</th>
                                        <td>{{ $warehouse->created_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                    
                        <google-map name="example" lat="{{ $warehouse->latitude }}" lng="{{ $warehouse->longitude }}"></google-map>

                    </div>
                </div>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Grading History</h3>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Season</th>
                            <th>Grade</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i = 1 @endphp
                        @foreach($warehouse->grades as $grade)
                            <tr>
                                <td>{{ $i++ }}.</td>
                                <td>{{ $grade->season->name }}</td>
                                <td>{{ $grade->grade->name }}</td>
                                <td>{{ $grade->status }}</td>
                                <td>{{ $grade->created_at }}</td>
                            </tr>
                        @endforeach            
                    </tbody>
                </table>
                <!-- /.table-responsive -->                
            </div>
            <div class="panel-footer">
                <a class="btn btn-primary" data-toggle="modal" href='#grade-warehouse-{{ $warehouse->id }}'>Grade this Warehouse</a>
                <div class="modal fade" id="grade-warehouse-{{ $warehouse->id }}">
                    <div class="modal-dialog">

                        <form action="{{ route('dashboard.warehouses.storegrades', $warehouse->id) }}" method="post">
                        {{ csrf_field() }}

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Grade {{ $warehouse->name }}</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="form-group">
                                       <label for="grade">Select Grade</label>
                                       <select name="grade" id="grade" class="form-control" required>
                                          <option value="">-Select-</option>
                                          @foreach($grades as $grade)
                                          <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                                          @endforeach
                                      </select>
                                    </div>  

                                      <div class="form-group">
                                       <label for="season">Select Season</label>
                                       <select name="season" id="season" class="form-control" required>
                                          <option value="">-Select-</option>
                                          @foreach($seasons as $season)
                                          <option value="{{ $season->id }}">{{ $season->name }}</option>
                                          @endforeach
                                      </select>
                                      </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
