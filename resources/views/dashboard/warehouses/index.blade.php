@extends('layouts.dashboard')

@section('title','Warehouses');

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Warehouses</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                All
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                   <table width="100%" class="table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Operator</th>
                            <th>Owner</th>
                            <th>Commodity</th>
                            <th>Capacity</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($warehouses as $warehouse)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                            <a href="{{ route('dashboard.warehouses.show', $warehouse->id) }}">
                                    {{ $warehouse->name }} 
                                </a>
                            </td>
                            <td>{{ $warehouse->operator->business_name }}</td>
                            <td>{{ $warehouse->owner->name }}</td>
                            <td>{{ $warehouse->commodity->name }}</td>
                            <td>{{ $warehouse->capacity }}</td>
                            <td>{{ $warehouse->status }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                      
                    </tbody>
                </table>  
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <a href="{{ route('dashboard.warehouses.create') }}" class="btn btn-primary">New</a>
        </div>
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>


@endsection
