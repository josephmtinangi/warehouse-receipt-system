@extends('layouts.dashboard')

@section('title', 'New main party')

@section('content')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">New main party</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New main party
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <form action="{{ route('main-parties.store') }}" method="post" role="form">
                                {{ csrf_field() }}

                                @include('dashboard.main-parties._form')

                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>

                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection
