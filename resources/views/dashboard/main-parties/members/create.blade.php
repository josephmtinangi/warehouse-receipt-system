@extends('layouts.dashboard')

@section('title', 'New ' . $mainParty->full_business_name . ' Member')

@section('content')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">New {{ $mainParty->full_business_name }} member</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New member
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            @include('errors.list')

                            <form action="{{ route('dashboard.main-parties.main-party.members.store', $mainParty->id) }}"
                                  method="post" role="form">
                                {{ csrf_field() }}

                                @include('dashboard.main-parties.members._form')

                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>

                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection
