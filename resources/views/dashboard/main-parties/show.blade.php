@extends('layouts.dashboard')

@section('title', $mainParty->full_business_name)

@section('content')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $mainParty->full_business_name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    About
                </div>
                <div class="panel-body">
                    {{ $mainParty }}
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Members ({{ $mainParty->members()->count() }})</h3>
                </div>
                <div class="panel-body">
                    {{ $mainParty->members }}
                </div>
                <div class="panel-footer">
                    <a href="{{ route('dashboard.main-parties.main-party.members.create', $mainParty->id) }}"
                       class="btn btn-primary">Add New</a>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection
