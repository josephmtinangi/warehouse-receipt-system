@extends('layouts.dashboard')

@section('title', 'Main Parties')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Main Parties</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">All Main Parties</h3>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Full business name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Phone number</th>
                            <th>Commodity</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i = 1 @endphp
                        @foreach($mainParties as $mainParty)
                            <tr>
                                <td>{{ $i++ }}.</td>
                                <td>
                                    <a href="{{ route('main-parties.show', $mainParty->id) }}">
                                        {{ $mainParty->full_business_name }}
                                    </a>
                                </td>
                                <td>{{ $mainParty->address }}</td>
                                <td>{{ $mainParty->email }}</td>
                                <td>{{ $mainParty->phone_number }}</td>
                                <td>{{ $mainParty->commodity_id }}</td>
                                <td>{{ $mainParty->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                    <a href="{{ route('main-parties.create') }}" class="btn btn-primary">New</a>
                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection
