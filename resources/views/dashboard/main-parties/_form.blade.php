<div class="form-group">
    <label for="commodity_id">Commodity</label>
    <select name="commodity_id" id="commodity_id" class="form-control" required>
        <option value="">-Select-</option>
        @foreach($commodities as $commodity)
            <option value="{{ $commodity->id }}">{{ $commodity->name }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="full_business_name">Full business name</label>
    <input type="text" name="full_business_name" id="full_business_name" class="form-control" required>
</div>
<div class="form-group">
    <label for="address">Address</label>
    <input type="text" name="address" id="address" class="form-control" required>
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="email" name="email" id="email" class="form-control" required>
</div>
<div class="form-group">
    <label for="phone_number">Phone</label>
    <input type="tel" name="phone_number" id="phone_number" class="form-control" required>
</div>
