@extends('layouts.dashboard')

@section('title', $warehouse->name . ' Farmers')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <a href="{{ route('warehouses.show', $warehouse->id) }}">
                {{ $warehouse->name }}
            </a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $warehouse->name }} Farmers
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Full name</th>
                            <th>Items</th>
                            <th>Username</th>
                            <th>Phone number</th>
                            <th>Address</th>
                            <th>Added</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($warehouse->farmers as $farmer)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('warehouses.farmers.show', [$warehouse->id, $farmer->id]) }}">
                                    {{ $farmer->full_name }}
                                </a>
                            </td>
                            <td>{{ $farmer->warehouseItems()->count() }}</td>
                            <td>{{ $farmer->username }}</td>
                            <td>{{ $farmer->phone_number }}</td>
                            <td>{{ $farmer->address }}</td>
                            <td>{{ $farmer->created_at }}</td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer">
                <a href="{{ route('warehouses.farmers.create', $warehouse->id) }}" class="btn btn-primary">Add Farmer</a>
            </div>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


@endsection
