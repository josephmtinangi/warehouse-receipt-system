@extends('layouts.dashboard')

@section('title','Create item')

@section('content')
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
           Create new item
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            New Item
                        </h3>
                    </div>
                    <div class="panel-body">

                        @include('errors.list')
                        
                        <form action="{{ route('warehouses.farmers.items.store', [$warehouse->id, $farmer->id]) }}" method="post">
                            {{ csrf_field() }}

                            @include('warehouse.warehouses.farmers.items._form')

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
