<div class="form-group">
    <label for="full_name">Full name</label>
    <input type="text" name="full_name" id="full_name" value="{{ old('full_name') }}" class="form-control" required>
</div>
<div class="form-group">
    <label for="phone_number">Phone number</label>
    <input type="text" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" class="form-control" required>
</div>
<div class="form-group">
    <label for="address">Address</label>
    <input type="text" name="address" id="address" value="{{ old('address') }}" class="form-control" required>
</div>
  