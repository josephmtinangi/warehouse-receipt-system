@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <a href="{{ route('warehouses.show', $warehouse->id) }}">
                {{ $warehouse->name }}
            </a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-body">
                                <a href="{{ route('warehouses.show', $warehouse->id) }}">
                                    {{ $warehouse->name }}
                                </a>
                                <h3>{{ $farmer->full_name }}</h3>
                                <p class="lead">{{ $items = $farmer->warehouseItems()->count() }} {{ str_plural('Item', $items) }}</p>
                                <a href="{{ route('warehouses.farmers.items.create', [$warehouse->id, $farmer->id]) }}" class="btn btn-primary pull-right">Add Item</a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Items ({{ $farmer->warehouseItems()->count() }})
                        </h3>
                    </div>
                    <div class="panel-body">
                       @if($farmer->warehouseItems()->count())
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Commodity</th>
                                            <th>Origin</th>
                                            <th>Quantity</th>
                                            <th>Weight</th>
                                            <th>Grade</th>
                                            <th>Class</th>
                                            <th>Moisture</th>
                                            <th>Depositor</th>
                                            <th>Receiver</th>
                                            <th>Date</th>
                                            <th>Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @foreach($farmer->warehouseItems as $item)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{ $item->commodity->name }}</td>
                                                <td>{{ $item->commodity_origin }}</td>
                                                <td>{{ $item->quantity }}</td>
                                                <td>{{ $item->weight }}</td>
                                                <td>{{ $item->grade }}</td>
                                                <td>{{ $item->class }}</td>
                                                <td>{{ $item->moisture }}</td>
                                                <td>{{ $item->depositor_user_id }}</td>
                                                <td>{{ $item->receiver_user_id }}</td>
                                                <td>{{ $item->created_at }}</td>
                                                <td>
                                                    <a href="{{ route('warehouses.items.show', [$warehouse->id, $item->id]) }}">
                                                        Click to view
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                       @else
                            <p class="text-center">No item</p>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
