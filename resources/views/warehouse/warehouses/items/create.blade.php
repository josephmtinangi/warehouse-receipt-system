@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <a href="{{ route('warehouses.show', $warehouse->id) }}">
                {{ $warehouse->name }}
            </a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Add Item
                        </h3>
                    </div>
                    <div class="panel-body">
                        
                        <form action="{{ route('warehouses.items.store', $warehouse->id) }}" method="post">
                            {{ csrf_field() }}

                            @include('warehouse.warehouses.items._form')

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
