@extends('layouts.dashboard')

@section('title', $warehouse->name)

@section('content')
<style type="text/css">
    .table,tr,td{
    border: 2px solid black;
    }
</style>
    <div class="row">
        <div class="col-lg-11 col-lg-offset-0" style="border: 5px double;">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <strong class="pull-right">Certificate of Title</strong>
                    <h2>WAREHOUSE RECEIPT</h2>
                    <span style="font-size:18px;"><b>(Warehouse Receipt Act No 10 of 2005)</b></span>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-lg-4 text-center">
                    <img src="{{asset('images/wrrb_logo.jpg')}}" alt="">
                </div>
                <div class="col-lg-4 text-center">
                    <img src="{{asset('images/government_logo.jpg')}}" alt="">
                </div>
                <div class="col-lg-4">
                    <p>
                        Date of issue __________20_____________
                    </p>
                    <p>
                        Warehouse No_______________________
                    </p>
                    <p>
                        Receipt No WRRB
                    </p>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-lg-12">
               <div class="col-lg-12 col-lg-offset-0">
                        <p>
                       By this Warehouse Receipt it is confirmed that the Warehouse
                       ________________________________________________________________<br>
                       <span style="margin-left:380px;"><i>(   Name of Warehouse Operator/Collateral Manager)</i></span>
                    </p>
                    <p>
                        Located in________________________________________________________________________________________________________<br>
                       <span style="margin-left:380px;"><i>(Physical Address,)</i></span>                    
                    </p>
                    <p>
                       Received for storing from___________________________________________________________________________________________<br>
                       <span style="margin-left:330px;"><i>(Name and Physical Address of the Depositor)</i></span>                    
                    </p>
                    <p>
                        Commodity with the following descriptions
                    </p>
               </div>
               
                    <table class="table ">
                        <tbody>
                        <tr>
                            <td colspan="1">Type(s) and Origin of the Commodity</td>
                            <td >Number of Package(s)</td>
                            <td>Physical Weight (kg)</td>
                            <td>Grade</td>
                            <td>Class</td>
                            <td>Moisture(%)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{{ $item->commodity_origin }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->weight }}</td>
                            <td>{{ $item->grade }}</td>
                            <td>{{ $item->class }}</td>
                            <td>{{ $item->moisture }}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                Physical weight in words
                            </td>
                            <td colspan="7">

                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <ol>
                        <li>
                               <p>
                      The Commodity are fully insured according to Insurance Policy No
                       _____________________of_____________________________________<br>
                       <span style="margin-left:440px;"><i>(Insurer)</i></span>
                    </p>
                        </li>
                        <li>
                                         <p>
                     The Nature and facts of Ownerships of the Commodity:
                       ___________________________________________________________________<br>
                       <span style="margin-left:440px;"><i><strong>(Solely / Jointly or Commonly Owned)</strong></i></span>
                    </p>
                        </li>
                        <li>
                            Warehouse Operator hereby undertakes to store the Commodity:
                            <ol style="list-style-type:lower-roman">
                                <li>
                                                  <p>
                     In quality and quantity as above mentioned until
                       ____________________________________________________after making due<br><span style="margin-left:370px;"><strong>(Specify Date)</strong></span><br>
                        allowances for the deterioration of_______________________of quality and weight loss__________________as specified herein.<br>
                       <p style="margin-left:250px;"><i>(Class) <span style="margin-left:300px;">(%)</span></p>
                    </p>
                                </li>
                                <li>With no financial interest in the Commodity covered by this receipt except a lien on the Commodity.</li>
                                <li>For a fee of Tshs____________________________________as lien until_______________________<br><p style="margin-left:200px;"><i>(Amount) <span style="margin-left:240px;">(Date)</span></p>
                                </li>
                            </ol>
                        </li>
                        <li>
                          The Holder of this Warehouse Receipt hereby undertakes:
                           <ol style="list-style-type:lower-roman">
                           <li>To pay the Warehouse Operator the specified Fee as lien</li>
                           <li>To inform the Warehouse Operator of any mis-delivery or liabilities incurred by use of this receipt.</li>
                           </ol>
                        </li>
                    </ol>
                    <br><br>
                    <p>Depositor’s Full Name____________________________________________Signature_________________________________________<br><br>
                    Authorized person of the Warehouse_________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_________________________________________
                    <br><p style="margin-left:270px;"><i>(Family name, position) <span style="margin-left:240px;">(Stamp & Signature)</span></p></p>
                </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->

    <br><br>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-4">
            <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-file-pdf-o"></i> Save as pdf</button>
        </div>
    </div>

    <br><br>

@endsection
