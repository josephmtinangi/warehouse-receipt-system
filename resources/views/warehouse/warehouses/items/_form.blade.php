<div class="form-group">
	<label for="commodity_id">Commodity</label>
	<select name="commodity_id" id="commodity_id" class="form-control" required>
		<option value="">-Select-</option>
		@foreach($commodities as $commodity)
			<option value="{{ $commodity->id }}">{{ $commodity->name }}</option>
		@endforeach
	</select>
</div>

<div class="form-group">
	<label for="commodity_origin">Commodity Origin</label>
	<input type="text" name="commodity_origin" id="commodity_origin" class="form-control" required>
</div>

<div class="form-group">
	<label for="quantity">Quantity</label>
	<input type="text" name="quantity" id="quantity" class="form-control" required>
</div>

<div class="form-group">
	<label for="weight">Weight</label>
	<input type="text" name="weight" id="weight" class="form-control" required>
</div>

<div class="form-group">
	<label for="grade">Grade</label>
	<select name="grade" id="grade" class="form-control" required="required">
		<option value=""></option>
	</select>
</div>

<div class="form-group">
	<label for="class">Class</label>
	<input type="text" name="class" id="class" class="form-control" required>
</div>
<div class="form-group">
	<label for="moisture">Moisture</label>
	<input type="text" name="moisture" id="moisture" class="form-control" required>
</div>
<div class="form-group">
	<label for="depositor">Depositor</label>
	<select name="depositor" id="depositor" class="form-control" required="required">
		<option value=""></option>
	</select>
</div>
<div class="form-group">
	<label for="group">Group</label>
	<select name="group" id="group" class="form-control" required="required">
		<option value=""></option>
	</select>
</div>
<div class="form-group">
	<label for="depositor_flag">Depositor flag</label>
	<input type="number" name="class" id="class" class="form-control" required>
</div>
<div class="form-group">
	<label for="property_one">Property one</label>
	<input type="text" name="property_one" id="property_one" class="form-control" required>
</div>
<div class="form-group">
	<label for="property_one">Property two</label>
	<input type="text" name="property_one" id="property_one" class="form-control" required>
</div>
<div class="form-group">
	<label for="property_one">Property three</label>
	<input type="text" name="property_one" id="property_one" class="form-control" required>
</div>

