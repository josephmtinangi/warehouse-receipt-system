@extends('layouts.dashboard')

@section('title', $item->commodity->name)

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $item->commodity->name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        {{ $item->commodity->name }}
                    </h3>
                </div>
                <div class="panel-body">
                
                    <dl class="dl-horizontal">
                        <dt>Commodity</dt>
                        <dd>{{ $item->commodity->name }}</dd>
                        <dt>Origin</dt>
                        <dd>{{ $item->commodity_origin }}</dd>
                        <dt>Quantity</dt>
                        <dd>{{ $item->quantity }}</dd>
                        <dt>Weight</dt>
                        <dd>{{ $item->weight }}</dd>
                        <dt>Grade</dt>
                        <dd>{{ $item->grade }}</dd>
                        <dt>Moisture</dt>
                        <dd>{{ $item->moisture }}</dd>
                        <dt>Property one</dt>
                        <dd>{{ $item->property_one }}</dd>
                        <dt>Property two</dt>
                        <dd>{{ $item->property_two }}</dd>
                        <dt>Property three</dt>
                        <dd>{{ $item->property_three }}</dd>
                        <dt>Property four</dt>
                        <dd>{{ $item->property_four }}</dd>
                        <dt>Date</dt>
                        <dd>{{ $item->created_at }}</dd>
                    </dl>

                </div>
                <div class="panel-footer">
                    <a href="{{ route('warehouses.items.receipts.create', [$warehouse->id, $item->id]) }}" class="btn btn-success">Generate Receipt</a>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    @include('errors.list')
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Transfer to individual</h3>
                        </div>
                        <div class="panel-body">
                            
                            <form action="{{ route('warehouses.items.item.individual-transfer', [$warehouse->id, $item->id]) }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="owner_user_id">Select user</label>
                                    <select name="owner_user_id" id="owner_user_id" class="form-control">
                                        <option value="">-Select-</option>
                                        @foreach($farmers as $farmer)
                                            <option value="{{ $farmer->id }}">{{ $farmer->full_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Transfer</button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Transfer to group</h3>
                        </div>
                        <div class="panel-body">

                            <form action="{{ route('warehouses.items.item.group-transfer', [$warehouse->id, $item->id]) }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="owner_group_id">Select group</label>
                                    <select name="owner_group_id" id="" class="form-control">
                                        <option value="">-Select-</option>
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }} - {{ $group->code }}</option>
                                        @endforeach                                        
                                    </select>
                                </div>
                                
                                <button type="submit" class="btn btn-primary">Transfer</button>
                            </form>


                        </div>
                    </div>
                </div>
            </div>            

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Ownership History</h3>
                </div>
                <div class="panel-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Item</th>
                                <th>Group</th>
                                <th>Farmer</th>
                                <th>Owner Flag</th>
                                <th>By</th>
                                <th>Date</th>
                                <th>Transfered</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1 @endphp
                            @foreach($item->ownerships as $ownership)
                            <tr>
                                <td>{{ $i++ }}.</td>
                                <td>{{ $ownership->item->commodity->name }}</td>
                                <td>
                                    @if($ownership->group)
                                        {{ $ownership->group->name }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($ownership->farmer)
                                        {{ $ownership->farmer->full_name }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{ $ownership->owner_flag }}</td>
                                <td>{{ $ownership->by->full_name }}</td>
                                <td>{{ $ownership->created_at->diffForHumans() }}</td>
                                <td>{{ $ownership->updated_at->diffForHumans() }}</td>
                                <td>{{ $ownership->status }}</td>
                            </tr>
                            @endforeach                        
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
