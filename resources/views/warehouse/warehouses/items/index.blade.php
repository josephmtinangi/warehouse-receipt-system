@extends('layouts.dashboard')

@section('title', $warehouse->name)

@section('content')


<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">{{ $warehouse->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Items
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Commodity</th>
                            <th>Origin</th>
                            <th>Quantity</th>
                            <th>Weight</th>
                            <th>Grade</th>
                            <th>Class</th>
                            <th>Moisture</th>
                            <th>Added</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i = 1 @endphp
                    @foreach($warehouse->items as $item)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('warehouses.items.show', [$warehouse->id, $item->id]) }}">
                                    {{ $item->commodity->name }}
                                </a>
                            </td>
                            <td>{{ $item->commodity_origin }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->weight }}</td>
                            <td>{{ $item->grade }}</td>
                            <td>{{ $item->class }}</td>
                            <td>{{ $item->moisture }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                    @endforeach                        
                    </tbody>
                </table>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
