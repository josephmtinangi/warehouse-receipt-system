@extends('layouts.dashboard')

@section('title', $warehouse->name)

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $warehouse->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $warehouse->name }}
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

             <dl class="dl-horizontal">
                <dt>Name</dt>
                <dd>{{ $warehouse->name }}</dd>
                <dt>Owner</dt>
                <dd>{{ $warehouse->owner->name }}</dd>
                <dt>Operator</dt>
                <dd>{{ $warehouse->operator->business_name }}</dd>
                <dt>Commodity</dt>
                <dd>{{ $warehouse->commodity->name }}</dd>
            </dl>

        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="panel-title" href="{{ route('warehouses.items.index', $warehouse->id) }}">
                Items
            </a>
            ({{ $warehouse->items()->count() }})
        </div>
        <div class="panel-body">

        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="panel-title" href="{{ route('warehouses.farmers.index', $warehouse->id) }}">
                Farmers
            </a>
            ({{ $warehouse->farmers()->count() }})
        </div>
        <div class="panel-body">

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="panel-title" href="{{ route('warehouses.primary-parties.index', $warehouse->id) }}">
                Primary Parties
            </a>
            ({{ $warehouse->primaryParties()->count() }})
        </div>
        <div class="panel-body">

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Receipts (0)</h3>
        </div>
        <div class="panel-body">

        </div>
    </div>
</div>
<!-- /.col-lg-12 -->
</div>


@endsection
