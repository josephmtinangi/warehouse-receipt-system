@extends('layouts.dashboard')

@section('title', 'Warehouses');

@section('content')

<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Warehouses</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                All Warehouses
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Operator</th>
                            <th>Location</th>
                            <th>Commodity</th>
                            <th>Click to view</th>
                          <!--   <th>Owner</th> -->
                         <!--    <th>Capacity</th>
                         <th>File number</th>
                         <th>(Lat, Lng)</th>
                         <th>Status</th>
                         <th>Created</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach(Auth::user()->director->operator->warehouses as $warehouse)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('warehouses.show', $warehouse->id) }}">
                                    {{ $warehouse->name }}
                                </a>
                            </td>
                            <td>{{ $warehouse->operator->business_name }}</td>
                            <td>
                                {{ $warehouse->location->region->name }},
                                {{ $warehouse->location->district->name }}
                            </td>
                            <td>{{ $warehouse->commodity->name }}</td>
                     <!--        <td>{{ $warehouse->owner->name }}</td>
                     <td>{{ $warehouse->capacity }}</td>
                     <td>{{ $warehouse->file_number }}</td>
                     <td>
                         (
                         {{ $warehouse->latitude }}&deg;,
                         {{ $warehouse->longitude }}&deg;
                         )
                     </td> -->
                           <!--  <td>{{ $warehouse->status }}</td>
                           <td>{{ $warehouse->created_at }}</td> -->
                           <td>
                               <a href="{{route('warehouses.viewdetails',$warehouse)}}"><span class="fa fa-eye"></span>&nbsp;More details</a>
                           </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>



@endsection
