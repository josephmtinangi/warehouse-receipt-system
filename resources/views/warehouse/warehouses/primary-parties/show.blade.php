@extends('layouts.dashboard')

@section('title', $warehouse->name . ' Farmers')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
         Primary parties
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-body">
                                <a href="{{ route('warehouses.show', $warehouse->id) }}">
                                    {{ $warehouse->name }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $primaryParty->full_business_name }}
                        </h3>
                    </div>
                    <div class="panel-body">
                      
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="panel-title" href="#">
                            Members
                        </a>
                        ({{ $primaryParty->members()->count() }})
                    </div>
                    <div class="panel-body">
                        {{--  --}}
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="panel-title" href="{{ route('warehouses.primary-parties.primary-party.groups.index', [$warehouse->id, $primaryParty->id]) }}">
                            Groups
                        </a>
                        ({{ $primaryParty->groups()->count() }})
                    </div>
                    <div class="panel-body">
                        {{--  --}}
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="panel-title" href="#">
                            Items
                        </a>
                        (NaN)
                    </div>
                    <div class="panel-body">
                        {{--  --}}
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Primary Items</h3>
                    </div>
                    <div class="panel-body">
                        {{--  --}}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
