@extends('layouts.dashboard')

@section('title', $warehouse->name . 'Primary party')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
         Primary parties
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-body">
                                <a href="{{ route('warehouses.show', $warehouse->id) }}">
                                    {{ $warehouse->name }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Primary Parties ({{ $warehouse->primaryParties()->count() }})
                        </h3>
                    </div>
                    <div class="panel-body">
                       @if($warehouse->primaryParties()->count())
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Full business name</th>
                                            <th>Address</th>
                                            <th>Email</th>
                                            <th>Phone number</th>
                                            <th>Commodity</th>
                                            <th>Joined</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @foreach($warehouse->primaryParties as $primaryParty)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>
                                                    <a href="{{ route('warehouses.primary-parties.show', [$warehouse->id, $primaryParty->id]) }}">
                                                        {{ $primaryParty->full_business_name }}
                                                    </a>
                                                </td>
                                                <td>{{ $primaryParty->address }}</td>
                                                <td>{{ $primaryParty->email }}</td>
                                                <td>{{ $primaryParty->phone_number }}</td>
                                                <td>{{ $primaryParty->commodity->name }}</td>
                                                <td>{{ $primaryParty->pivot->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                       @else
                            <p class="text-center">No Primary Party</p>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
