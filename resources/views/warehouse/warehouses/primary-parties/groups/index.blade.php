@extends('layouts.dashboard')

@section('title', $warehouse->name . ' Group')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <a href="{{ route('warehouses.show', $warehouse->id) }}">
                {{ $warehouse->name }}
            </a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-body">
                                <a href="{{ route('warehouses.show', $warehouse->id) }}">
                                    {{ $warehouse->name }}
                                </a>
                                /
                                {{ $primaryParty->full_business_name }}
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Groups ({{ $primaryParty->groups()->count() }})
                        </h3>
                    </div>
                    <div class="panel-body">
                        @if($primaryParty->groups()->count())
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Primary Items</th>
                                            <th>Secondary Items</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @foreach($primaryParty->groups as $group)
                                        <tr>
                                            <td>{{ $i++ }}.</td>
                                            <td>
                                                <a href="{{ route('warehouses.primary-parties.primary-party.groups.show', [$warehouse->id, $primaryParty->id, $group->id]) }}">
                                                    {{ $group->name }}
                                                </a>
                                            </td>
                                            <td>{{ $group->code }}</td>
                                            <td>{{ $group->items()->count() }}</td>
                                            <td>NaN</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p class="text-center">No group</p>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
