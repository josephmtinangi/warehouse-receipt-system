@extends('layouts.dashboard')

@section('title', 'Item create')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <a href="{{ route('warehouses.show', $warehouse->id) }}">
                {{ $warehouse->name }}
            </a>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
                    <div class="col-sm-12">
                        <div class="panel">
                            <div class="panel-body">
                                <a href="{{ route('warehouses.show', $warehouse->id) }}">
                                    {{ $warehouse->name }}
                                </a>
                                /
                                {{ $primaryParty->full_business_name }}
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Items</h3>
                    </div>
                    <div class="panel-body">

                        @include('errors.list')
                        
                        <form action="{{ route('warehouses.primary-parties.primary-party.groups.items.store', [$warehouse->id, $primaryParty->id, $primaryPartyGroup->id]) }}" method="post">
                            {{ csrf_field() }}

                            @include('warehouse.warehouses.primary-parties.groups.items._form')

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
