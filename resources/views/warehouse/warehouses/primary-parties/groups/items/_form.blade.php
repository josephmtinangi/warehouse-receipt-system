<div class="form-group">
	<label for="commodity_id">Commodity</label>
	<select name="commodity_id" id="commodity_id" class="form-control" required>
		<option value="">-Select-</option>
		@foreach($commodities as $commodity)
			<option value="{{ $commodity->id }}">{{ $commodity->name }}</option>
		@endforeach
	</select>
</div>

<div class="form-group">
	<label for="commodity_origin">Commodity Origin</label>
	<input type="text" name="commodity_origin" id="commodity_origin" class="form-control" required>
</div>

<div class="form-group">
	<label for="quantity">Quantity</label>
	<input type="number" name="quantity" id="quantity" class="form-control" required>
</div>

<div class="form-group">
	<label for="weight">Weight</label>
	<input type="number" name="weight" id="weight" class="form-control" required>
</div>

<div class="form-group">
	<label for="grade">Grade</label>
	<input type="text" name="grade" id="grade" class="form-control" required>
</div>

<div class="form-group">
	<label for="class">Class</label>
	<input type="text" name="class" id="class" class="form-control" required>
</div>
<div class="form-group">
	<label for="moisture">Moisture</label>
	<input type="text" name="moisture" id="moisture" class="form-control" required>
</div>
<div class="form-group">
	<label for="property_one">Property one</label>
	<input type="text" name="property_one" id="property_one" class="form-control">
</div>
<div class="form-group">
	<label for="property_two">Property two</label>
	<input type="text" name="property_two" id="property_two" class="form-control">
</div>
<div class="form-group">
	<label for="property_three">Property three</label>
	<input type="text" name="property_three" id="property_three" class="form-control">
</div>
<div class="form-group">
	<label for="property_four">Property four</label>
	<input type="text" name="property_four" id="property_four" class="form-control">
</div>
