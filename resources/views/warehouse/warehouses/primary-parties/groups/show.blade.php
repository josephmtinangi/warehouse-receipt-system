@extends('layouts.dashboard')

@section('title', $warehouse->name . ' Group')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
           Primary parties
       </h1>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
    <div class="panel panel-default">
            <div class="panel-body">
                <a href="{{ route('warehouses.show', $warehouse->id) }}">
                    {{ $warehouse->name }}
                </a>
                /
                {{ $primaryParty->full_business_name }}
            </div>
        </div>
    </div>
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                {{ $primaryPartyGroup->name }} - 
                {{ $primaryPartyGroup->code }} 
            </h3>
        </div>
        <div class="panel-body">
         <p class="lead">Items ({{ $primaryPartyGroup->secondaryItems()->count() }})</p> <a href="{{ route('warehouses.primary-parties.primary-party.groups.items.create', [$warehouse->id, $primaryParty->id, $primaryPartyGroup->id]) }}" class="btn btn-primary pull-right">Add Item</a>
     </div>
 </div>

 <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Items ({{ $primaryPartyGroup->secondaryItems()->count() }})</h3>
    </div>
    <div class="panel-body">
     @if($primaryPartyGroup->secondaryItems()->count())
     <div class="table-responsive">
        <table class="table table-hover table-bordered table-condensed">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>Commodity</th>
                    <th>Origin</th>
                    <th>Quantity</th>
                    <th>Weight</th>
                    <th>Grade</th>
                    <th>Class</th>
                    <th>Moisture</th>
                    <th>Depositor</th>
                    <th>Receiver</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 1 @endphp
                @foreach($primaryPartyGroup->secondaryItems as $item)
                <tr>
                    <td>{{ $i++ }}.</td>
                    <td>{{ $item->commodity->name }}</td>
                    <td>{{ $item->commodity_origin }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ $item->weight }}</td>
                    <td>{{ $item->grade }}</td>
                    <td>{{ $item->class }}</td>
                    <td>{{ $item->moisture }}</td>
                    <td>{{ $item->depositor_user_id }}</td>
                    <td>{{ $item->receiver_user_id }}</td>
                    <td>{{ $item->created_at }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @else
    <p class="text-center">No item</p>
    @endif                        
</div>
</div>

</div>
</div>
</div>
@endsection
