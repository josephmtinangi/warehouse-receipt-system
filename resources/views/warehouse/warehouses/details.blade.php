@extends('layouts.dashboard')

@section('title', $warehouse->name)

@section('content')


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $warehouse->name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $warehouse->name }}
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

             <dl class="dl-horizontal">
                <dt>Name</dt>
                <dd>{{ $warehouse->name }}</dd>
                <dt>Owner</dt>
                <dd>{{ $warehouse->owner->name }}</dd>
                <dt>Operator</dt>
                <dd>{{ $warehouse->operator->business_name }}</dd>
                <dt>Commodity</dt>
                <dd>{{ $warehouse->commodity->name }}</dd>
                <dt>Capacity</dt>
                <dd>{{ $warehouse->capacity }}</dd>
                <dt>File number</dt>
                <dd>{{ $warehouse->commodity->name }}</dd>
                <dt>(Lat, Lng)</dt>
                <dd>
                  {{ $warehouse->latitude }}&deg;,
                  {{ $warehouse->longitude }}&deg;
                </dd>
                <dt>Status</dt>
                <dd>{{ $warehouse->status }}</dd>
                <dt>Location</dt>
                <dd>{{ $warehouse->location->region->name }},{{ $warehouse->location->district->name }}</dd>
                <dt>Registered</dt>
                <dd>{{ $warehouse->created_at }}</dd>
            </dl>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <span>Warehouse seasonal grading</span>
            </div>
            <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Season</th>
                            <th>Grade</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i = 1 @endphp
                        @foreach($warehouse->grades as $grade)
                            <tr>
                                <td>{{ $grade->season->name }}</td>
                                <td>{{ $grade->grade->name }}</td>
                                <td>{{ $grade->created_at }}</td>
                            </tr>
                        @endforeach            
                    </tbody>
                </table>
                <!-- /.table-responsive -->   
            </div>
        </div>     
        <!-- /.panel-body -->
    </div>
</div>
<!-- /.col-lg-12 -->
</div>


@endsection
