 <ul class="nav" id="side-menu">
  	    <li>
        <a href="#">Parties<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
           <li>
            <a href="{{ route('home') }}">Primary parties</a>
        </li>
        <li>
            <a href="#">Main parties</a>
        </li>
    </ul>
    <!-- /.nav-second-level -->
</li>
 	    <li>
        <a href="#">Items<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
           <li>
            <a href="{{ route('primaryItems.index') }}">Primary items</a>
        </li>
        <li>
            <a href="{{ route('secondaryItems.index') }}">Warehouse items</a>
        </li>
          <li>
            <a href="#">Items Status</a>
        </li>
    </ul>
    <!-- /.nav-second-level -->
</li>
 <li>
        <a href="#">Auctions<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
           <li>
            <a href="#">Previous auctions</a>
        </li>
        <li>
            <a href="#">Upcomming auctions</a>
        </li>
          <li>
            <a href="#">Indicative price</a>
        </li>
    </ul>
    <!-- /.nav-second-level -->
</li>

 </ul>