 <ul class="nav" id="side-menu">
 	<li>
 		<a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> Home</a>
 	</li>
 	<li>
 		<a href="{{ route('warehouses.index') }}"><i class="fa fa-building-o fa-fw"></i> Warehouses</a>
 	</li>
 	<li>
 		<a href="{{ route('warehouses.items.index', Auth::user()->director->operator->warehouses()->first()->id) }}"><i class="fa fa-archive fa-fw"></i> Items</a>
 	</li>
 	<li>
 		<a href="{{ route('warehouses.farmers.index', Auth::user()->director->operator->warehouses()->first()->id) }}"><i class="fa fa-users fa-fw"></i> Farmers</a>
 	</li>
 	<li>
 		<a href="{{ route('warehouses.primary-parties.index', Auth::user()->director->operator->warehouses()->first()->id) }}"><i class="fa fa-group fa-fw"></i> Primary Parties</a>
 	</li>
 </ul>