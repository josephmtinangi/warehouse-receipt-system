<ul class="nav" id="side-menu">

    <li>
        <a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> Home</a>
    </li>
     <li>
        <a href="#"><i class="fa fa-users fa-fw"></i>Farmers</a>
    </li>
    <li>
        <a href="{{ route('main-party.main-party.primary-parties.index', Auth::user()->mainParties()->first()->id) }}"><i class="fa fa-key fa-fw"></i> Primary Parties</a>
    </li>
    <li>
        <a href="{{ route('main-party.main-party.members.index', Auth::user()->mainParties()->first()->id) }}"><i class="fa fa-users fa-fw"></i> Members</a>
    </li>

</ul>
