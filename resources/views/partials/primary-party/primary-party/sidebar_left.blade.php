<li>
    <a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> Home</a>
</li>
<li>
    <a href="{{ route('primary-party.primary-party.index') }}"><i class="fa fa-key fa-fw"></i> Primary Parties</a>
</li>
<li>
    <a href="#"><i class="fa fa-users fa-fw"></i> Members</a>
</li>
<li>
    <a href="#"><i class="fa fa-users fa-fw"></i> Groups</a>
</li>