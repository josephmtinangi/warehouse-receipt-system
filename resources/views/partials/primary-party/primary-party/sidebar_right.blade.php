<div class="panel panel-default">
    <div class="panel-heading">You</div>

    <div class="panel-body">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <h3>Role</h3>

        @isset(Auth::user()->role)

        {{ Auth::user()->role->name }}

        @endisset

        @empty(Auth::user()->role)
            -
            @endempty

    </div>
</div>