 <ul class="nav" id="side-menu">

 	<li>
 		<a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> Home</a>
 	</li>
	 <li>
	 <a href="{{ route('primary-party.primary-party.index') }}"><i class="fa fa-key fa-fw"></i> Primary Parties</a>
	 </li>
 	<li>
 		<a href="#"><i class="fa fa-users fa-fw"></i> Members</a>
 	</li>
 	<li>
 		<a href="#"><i class="fa fa-users fa-fw"></i> Groups</a>
 	</li>
     <li>
        <a href="#">Auctions<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
           <li>
            <a href="#">Previous auctions</a>
        </li>
        <li>
            <a href="#">Upcomming auctions</a>
        </li>
          <li>
            <a href="#">Indicative price</a>
        </li>
    </ul>
    <!-- /.nav-second-level -->
</li>
 </ul>
