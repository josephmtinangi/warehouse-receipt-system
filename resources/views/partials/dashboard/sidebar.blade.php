 <ul class="nav" id="side-menu">
    <li class="sidebar-search">
        <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        <!-- /input-group -->
    </li>
    <li>
        <a href="{{ route('dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
    </li>
    <li>
        <a href="{{ route('roles.index') }}"><i class="fa fa-shield fa-fw"></i> Roles</a>
    </li>
    <li>
        <a href="{{ route('positions.index') }}"><i class="fa fa-black-tie fa-fw"></i> Positions</a>
    </li>    
    <li>
        <a href="{{ route('users.index') }}"><i class="fa fa-users fa-fw"></i> Users</a>
    </li>
    <li>
        <a href="#"><i class="fa fa-map-marker fa-fw"></i>Manage Locations<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
           <li>
            <a href="{{ route('regions.index') }}"><i class="fa fa-map fa-fw"></i> Regions</a>
        </li>
        <li>
            <a href="{{ route('districts.index') }}"><i class="fa fa-map-o fa-fw"></i> Districts</a>
        </li>
        <li>
            <a href="{{ route('location.index') }}"><i class="fa fa-map-marker fa-fw"></i> Locations</a>
        </li>
    </ul>
    <!-- /.nav-second-level -->
</li>
<li>
<a href="{{ route('commodities.index') }}"><i class="fa fa-leaf fa-fw"></i> Commodities</a>
</li>
<li>
    <a href="{{ route('licenses.index') }}"><i class="fa fa-file-pdf-o fa-fw"></i> Licenses</a>
</li>
<li>
    <a href="{{ route('grades.index') }}"><i class="fa fa-book fa-fw"></i> Grades</a>
</li>
<li>
    <a href="{{ route('seasons.index') }}"><i class="fa fa-leaf fa-fw"></i> Seasons</a>
</li>
<li>
    <a href="#"><i class="fa fa-cogs fa-fw"></i>Manage Warehouses<span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
      <li>
        <a href="{{ route('warehouse-operators.index') }}"><i class="fa fa-institution fa-fw"></i> Operators</a>
    </li>
    <li>
        <a href="{{ route('warehouse-operator-directors.index') }}"><i class="fa fa-users fa-fw"></i> 
            Directors</a>
        </li>
        <li>
            <a href="{{ route('warehouses-owners.index') }}"><i class="fa fa-black-tie fa-fw"></i> Owners</a>
        </li>
        <li>
            <a href="{{ route('dashboard.warehouses.index') }}"><i class="fa fa-building fa-fw"></i> Warehouses</a>
        </li>
    </ul>
    <!-- /.nav-second-level -->
</li>
<li>
    <a href="{{ route('main-parties.index') }}"><i class="fa fa-institution fa-fw"></i> Main Parties</a>
</li>
<li>
    <a href="{{ route('primary-parties.index') }}"><i class="fa fa-group fa-fw"></i> Primary Parties</a>
</li>
<li>
    <a href="{{ route('banks.index') }}"><i class="fa fa-building fa-fw"></i> Banks</a>
</li>

</ul>