<div class="sidebar-nav navbar-collapse">

    @if(Auth::user()->isAdmin())

        @include('partials.dashboard.sidebar')

    @endif

    @if(Auth::user()->isDirector())

        @include('partials.warehouses.sidebar')

    @endif

    @if(Auth::user()->isMainParty())

        @include('partials.main-party.sidebar')

    @endif

    @if(Auth::user()->isPrimaryParty())
        @include('partials.primary-party.sidebar')
    @endif

    @if(Auth::user()->isFarmer())
        @include('partials.farmer.sidebar')
    @endif

</div>
<!-- /.sidebar-collapse -->
