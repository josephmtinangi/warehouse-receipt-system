@extends('layouts.dashboard')

@section('title', 'Secondary items')

@section('content')


<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Warehouse Items</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Items
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @if($items->count() > 0 )
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Commodity</th>
                            <th>Origin</th>
                            <th>Quantity</th>
                            <th>Weight</th>
                            <th>Grade</th>
                            <th>Class</th>
                            <th>Moisture</th>
                            <th>Added</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i = 1 @endphp
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="#">
                                    {{ $item->commodity->name }}
                                </a>
                            </td>
                            <td>{{ $item->commodity_origin }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->weight }}</td>
                            <td>{{ $item->grade }}</td>
                            <td>{{ $item->class }}</td>
                            <td>{{ $item->moisture }}</td>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                    @endforeach                        
                    </tbody>
                </table>
                <!-- /.table-responsive -->
                @else
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>No item found</strong>
                </div>
                @endif
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
