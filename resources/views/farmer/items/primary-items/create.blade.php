@extends('layouts.dashboard')

@section('title', 'New item');

@section('content')


<div class="row">
    <div class="col-lg-12">
    <h1 class="page-header">Add new primary item</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               New primary item
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">

                      <form action="{{ route('primaryItems.store') }}" method="post">
                        {{ csrf_field() }}

                        @include('farmer.items.primary-items._form')


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>


                </div>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


@endsection
