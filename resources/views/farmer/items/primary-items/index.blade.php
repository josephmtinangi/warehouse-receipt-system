@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Primary Items</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if(Auth::user()->primaryItems->count() > 0)
                    All farmer's primary items.
                    @else
                    <a href="{{ route('primaryItems.create') }}" class="btn btn-primary">New</a>
                    @endif
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @if(Auth::user()->primaryItems->count() > 0)
                         <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Commodity name</th>
                            <th>Quantity</th>
                            <th>weight</th>
                            <th>class</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $i = 1 @endphp
                        @foreach(Auth::user()->primaryItems as $primaryItem)
                            <tr>
                                <td>{{ $i++ }}.</td>
                                <td>
                                    <a href="{{ route('primary-party.primary-party.show', $primaryItem->id) }}">
                                        {{ $primaryItem->commodity->name }}
                                    </a>
                                </td>
                                <td>{{ $primaryItem->quantity }}</td>
                                <td>{{ $primaryItem->weight }}</td>
                                <td>{{ $primaryItem->class }}</td>
                                <td>{{ $primaryItem->created_at}}</td>
                                <td>
                                    <a href="#" class="fa fa-edit">Edit</a>
                                </td>
                                <td>
                                    <a href="#">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                    @else
                    <div class="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>No primary items found</strong> 
                    </div>
                    @endif
                </div>
                <!-- /.panel-body -->
                <div class="panel-footer">
                     @if(Auth::user()->primaryItems->count() > 0)
                     <a href="{{ route('primaryItems.create') }}" class="btn btn-primary">New</a>
                     @endif
                </div>
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection
