    <div class="form-group">
    	<label for="cheo">Position</label>
    	<select name="cheo" id="cheo" class="form-control" required>
    		<option value="">-Select-</option>
    		@foreach($positions as $position)
    		<option value="{{ $position->id }}">{{ $position->display_name }}</option>
    		@endforeach
    	</select>
    </div>

    <div class="form-group">
    	<label for="full_name">Full name</label>
    	<input type="text" name="full_name" id="full_name" value="{{ old('full_name') }}" class="form-control" required>
    </div>
    <div class="form-group">
    	<label for="phone_number">Phone number</label>
    	<input type="tel" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" class="form-control" required>
    </div>
    <div class="form-group">
    	<label for="username">Username</label>
    	<input type="text" name="username" id="username" {{ old('username') }} class="form-control" required>
    </div>
    <div class="form-group">
    	<label for="address">Address</label>
    	<input type="text" name="address" id="address" {{ old('address') }} class="form-control" required>
    </div>  