@extends('layouts.dashboard')

@section('title', $mainParty->full_business_name . 'New Exclusive member')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $mainParty->full_business_name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add exclusive member to {{ $mainParty->full_business_name }}
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            @include('errors.list')

                            <form action="{{ route('main-party.main-party.members.store', $mainParty->id) }}" method="post">
                                {{ csrf_field() }}

                                @include('main-party.main-party.members._form')

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>

                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection
