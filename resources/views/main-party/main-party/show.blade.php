@extends('layouts.dashboard')

@section('title', $mainParty->full_business_name)

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $mainParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-default">
            <div class="panel-heading">About</div>

            <div class="panel-body">

                <dl class="dl-horizontal">
                    <dt>Full business name</dt>
                    <dd>{{ $mainParty->full_business_name }}</dd>
                    <dt>Address</dt>
                    <dd>{{ $mainParty->address }}</dd>
                    <dt>Email</dt>
                    <dd>{{ $mainParty->email }}</dd>
                    <dt>Phone number</dt>
                    <dd>{{ $mainParty->phone_number }}</dd>
                    <dt>Members</dt>
                    <dd>{{ $mainParty->members()->count() }}</dd>
                </dl>

            </div>
        </div>

    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
