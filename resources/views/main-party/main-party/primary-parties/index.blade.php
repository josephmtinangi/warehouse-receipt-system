@extends('layouts.dashboard')

@section('title', $mainParty->full_business_name)

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $mainParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-default">
            <div class="panel-heading">Primary Parties ({{ $mainParty->primaryParties()->count() }})</div>

            <div class="panel-body">

                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr> 
                            <th>SN</th>
                            <th>Full business name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Phone number</th>
                            <th>Commodity</th>
                            <th>Created</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1 @endphp
                        @foreach($mainParty->primaryParties as $primaryParty)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('main-party.main-party.primary-parties.show', [$mainParty->id, $primaryParty->id]) }}">
                                    {{ $primaryParty->full_business_name }}
                                </a>
                            </td>
                            <td>{{ $primaryParty->address }}</td>
                            <td>{{ $primaryParty->email }}</td>
                            <td>{{ $primaryParty->phone_number }}</td>
                            <td>{{ $primaryParty->commodity->name }}</td>
                            <td>{{ $primaryParty->created_at }}</td>
                            <td><a href="#">Edit</a></td>
                            <td><a href="#">Delete</a></td>
                        </tr>
                        @endforeach                       
                    </tbody>
                </table>
                <!-- /.table-responsive -->

            </div>
            <div class="panel-footer">
                <a href="{{ route('main-party.main-party.primary-parties.create', $mainParty->id) }}" class="btn btn-primary">Create</a>
            </div>
        </div>

    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
