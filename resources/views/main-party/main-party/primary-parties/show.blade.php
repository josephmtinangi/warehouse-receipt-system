@extends('layouts.dashboard')

@section('title', $primaryParty->full_business_name)

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $primaryParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-default">
            <div class="panel-heading">About</div>

            <div class="panel-body">

                {{ $primaryParty }}

            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Members ({{ $primaryParty->members()->count() }})</h3>
            </div>
            <div class="panel-body">
                {{ $primaryParty->members }}
            </div>
            <div class="panel-footer">
                <a href="{{ route('main-party.main-party.primary-parties.members.create', [$mainParty->id, $primaryParty->id]) }}" class="btn btn-primary">Add member</a>
            </div>
        </div>

    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
