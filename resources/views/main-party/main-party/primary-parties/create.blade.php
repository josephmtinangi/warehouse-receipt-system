@extends('layouts.dashboard')

@section('title', 'New Primary Party')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $mainParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-default">
            <div class="panel-heading">New Primary Party</div>

            <div class="panel-body">


                <form action="{{ route('main-party.main-party.primary-parties.store', $mainParty->id) }}" method="post">
                    {{ csrf_field() }}

                    @include('main-party.main-party.primary-parties._form')

                    <button type="submit" class="btn btn-primary">Create</button>
                </form>              

            </div>
        </div>

    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
