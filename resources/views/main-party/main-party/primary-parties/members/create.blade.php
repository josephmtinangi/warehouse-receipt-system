@extends('layouts.dashboard')

@section('title', 'New Member')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{ $primaryParty->full_business_name }}</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-12">


        <div class="panel panel-default">
        <div class="panel-heading">New member</div>

            <div class="panel-body">


              <!-- Display Validation Errors -->
              @include('errors.list')

              <form action="{{ route('main-party.main-party.primary-parties.members.store', [$mainParty->id, $primaryParty->id]) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="cheo">Position</label>
                    <select name="cheo" id="cheo" class="form-control" required>
                        <option value="">-Select-</option>
                        @foreach($positions as $position)
                        <option value="{{ $position->id }}">{{ $position->display_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="full_name">Full name</label>
                    <input type="text" name="full_name" id="full_name" value="{{ old('full_name') }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="phone_number">Phone number</label>
                    <input type="tel" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" id="username" {{ old('username') }} class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" name="address" id="address" {{ old('address') }} class="form-control" required>
                </div>                            

                <button type="submit" class="btn btn-primary">Register</button>
            </form>             

        </div>
    </div>

</div>
<!-- /.col-lg-12 -->
</div>

@endsection
