<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'phone_number', 'username', 'password', 'address', 'role_id', 'status', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function mainParties()
    {
        return $this->belongsToMany(MainParty::class, 'chama_kikuu_exclusive_membership', 'user_id', 'chama_kikuu_id')
            ->withPivot('cheo')
            ->withTimestamps();
    }

    public function primaryParties()
    {
        return $this->belongsToMany(PrimaryParty::class, 'chama_cha_msingi_membership', 'chama_cha_msingi_id', 'user_id')
            ->withPivot('cheo')
            ->withTimestamps();
    }

    public function primaryItems()
    {
        return $this->hasMany(PrimaryItem::class, 'depositor_user_id');
    }

    public function secondaryItems()
    {
        return $this->hasMany(Item::class, 'depositor_user_id');
    }

    public function director()
    {
        return $this->hasOne(WarehouseOperatorDirector::class);
    }

    public function isDirector()
    {
        return null === $this->director ? false : true;
    }

    public function isPrimaryParty()
    {
        return $this->primaryParties()->count() > 0 ? true : false;
    }

    public function isMainParty()
    {
        return $this->mainParties()->count() > 0 ? true : false;
    }

    public function isAdmin()
    {
        $admin = Role::whereName('Admin')->first();
        if (null === $admin) {
            return false;
        }

        return $this->role->name === $admin->name;
    }

    public function isFarmer()
    {
        $farmer = Role::whereName('Farmer')->first();
        if (null === $farmer) {
            return false;
        }

        return $this->role_id === $farmer->id;
    }
}
