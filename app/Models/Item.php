<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function commodity()
    {
    	return $this->belongsTo(Commodity::class);
    }

    public function ownerships()
    {
    	return $this->hasMany(ItemOwnership::class);
    }
}
