<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemOwnership extends Model
{
    public function item()
    {
    	return $this->belongsTo(Item::class);
    }

    public function by()
    {
    	return $this->belongsTo(User::class, 'created_by');
    }

    public function farmer()
    {
    	return $this->belongsTo(User::class, 'owner_user_id');
    }

    public function group()
    {
    	return $this->belongsTo(PrimaryPartyGroup::class, 'owner_group_id');
    }
}
