<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseOperatorDirector extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function operator()
    {
    	return $this->belongsTo(WarehouseOperator::class, 'warehouse_operator_id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
