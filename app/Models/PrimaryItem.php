<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrimaryItem extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function commodity()
    {
    	return $this->belongsTo(Commodity::class);
    }

    public function depositor()
    {
    	return $this->belongsTo(User::class, 'depositor_user_id');
    }

    public function receiver()
    {
    	return $this->belongsTo(User::class, 'receiver_user_id');
    }
}
