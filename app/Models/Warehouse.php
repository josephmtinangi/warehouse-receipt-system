<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Warehouse extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function operator()
    {
        return $this->belongsTo(WarehouseOperator::class, 'warehouse_operator_id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function commodity()
    {
        return $this->belongsTo(Commodity::class);
    }

    public function owner()
    {
        return $this->belongsTo(WarehouseOwner::class, 'warehouse_owner_id');
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function farmers()
    {
        return $this->belongsToMany(User::class, 'farmers');
    }

    public function primaryParties()
    {
        return $this->belongsToMany(PrimaryParty::class)->withTimestamps();
    }

    public function grades()
    {
        return $this->hasMany(WarehouseGrade::class);
    }
}
