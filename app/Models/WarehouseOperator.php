<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseOperator extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function warehouses()
    {
    	return $this->hasMany(Warehouse::class);
    }
}
