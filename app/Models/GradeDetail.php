<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GradeDetail extends Model
{
 	use SoftDeletes;

 	protected $dates = ['deleted_at'];

 	public function warehouseGrades()
 	{
 		return $this->hasMany(WarehouseGrade::class);
 	}
}
