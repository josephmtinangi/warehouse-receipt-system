<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];


    public function region()
    {
    	return $this->belongsTo(Region::class);
    }

    public function district()
    {
    	return $this->belongsTo(District::class);
    }
}
