<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrimaryPartyGroup extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'chama_cha_msingi_group';

    public function members()
    {
    	return $this->belongsToMany(User::class, 'chama_cha_msingi_group_membership', 'group_id', 'user_id');
    }

    public function items()
    {
    	return $this->belongsToMany(PrimaryItem::class, 'chama_cha_msingi_group_primary_items', 'group_id', 'primary_item_id');
    }

    public function secondaryItems()
    {
    	return $this->hasMany(Item::class, 'group_id');
    }
}
