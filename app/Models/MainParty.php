<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MainParty extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'chama_kikuu';

    public function primaryParties()
    {
        return $this->belongsToMany(PrimaryParty::class, 'chama_kikuu_membership', 'chama_kikuu_id', 'chama_cha_msingi_id')
            ->withTimestamps();
    }

    public function members()
    {
        return $this->belongsToMany(User::class, 'chama_kikuu_exclusive_membership', 'chama_kikuu_id', 'user_id')
            ->withPivot('cheo')
            ->withTimestamps();
    }
}
