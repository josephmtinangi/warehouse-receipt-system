<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseGrade extends Model
{
 	use SoftDeletes;

 	protected $dates = ['deleted_at'];

 	public function grade()
 	{
 		return $this->belongsTo(GradeDetail::class, 'grade_detail_id');
 	}

 	public function season()
 	{
 		return $this->belongsTo(SeasonDetail::class, 'season_detail_id');
 	}
}
