<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrimaryParty extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'chama_cha_msingi';

    // Primary party has one commodity
    public function commodity()
    {
        return $this->belongsTo(Commodity::class);
    }

    // primary party has many members
    public function members()
    {
        return $this->belongsToMany(User::class, 'chama_cha_msingi_membership', 'user_id', 'chama_cha_msingi_id')
            ->withPivot('cheo')
            ->withTimestamps();
    }

    // primary party has many groups
    public function groups()
    {
        return $this->belongsToMany(PrimaryPartyGroup::class, 'chama_cha_msingi_group_membership', 'group_id', 'user_id');
    }
}
