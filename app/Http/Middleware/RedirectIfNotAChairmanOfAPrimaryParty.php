<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RedirectIfNotAChairmanOfAPrimaryParty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect()->route('login');
        }

        if (Auth::user()->primaryParties()->count() === 0) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
