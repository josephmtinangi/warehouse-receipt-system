<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Closure;

class RedirectIfNotAFarmer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect()->login('login');
        }

        $role = Role::firstOrCreate(['name' => 'Farmer']);

        if (Auth::user()->role) {
            if (Auth::user()->role->id !== $role->id) {
                return redirect()->route('home');
            }
        }

        return $next($request);
    }
}
