<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\Models\Role;

class RedirectIfNotAnAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect()->route('login');
        }

        $admin = Role::whereName('Admin')->first();
        if (null === $admin) {
            return redirect()->route('home');
        }

        $user = $admin->users()->find(auth()->id());

        if (null === $user) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
