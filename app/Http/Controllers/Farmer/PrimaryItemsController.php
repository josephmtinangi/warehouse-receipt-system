<?php

namespace App\Http\Controllers\Farmer;

use App\Models\Commodity;
use App\Models\PrimaryItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrimaryItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   
    $primaryParties = PrimaryItem::latest()->get();
      return view('farmer.items.primary-items.index',compact('primaryItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commodities = Commodity::orderBy('name')->get();
        return view('farmer.items.primary-items.create',compact('commodities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request, [
            'commodity_id' => 'required',
            'quantity' => 'required',
            'weight' => 'required',
            'grade' => 'required',
            'class' => 'required',
        ]);

        $primaryItem = new PrimaryItem;
        $primaryItem->commodity_id = $request->input('commodity_id');
        $primaryItem->quantity = $request->input('quantity');
        $primaryItem->weight = $request->input('weight');
        $primaryItem->grade = $request->input('grade');
        $primaryItem->class = $request->input('class');
        $primaryItem->depositor_user_id = auth()->id();; 
        $primaryItem->class = $request->input('class');
        $primaryItem->save();

        flash('Item added successful.')->success()->important();

        return redirect()->route('primaryItems');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
