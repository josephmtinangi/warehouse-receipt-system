<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\GradeDetail;
use App\Models\SeasonDetail;
use App\Models\Commodity;
use App\Models\Location;
use App\Models\Warehouse;
use App\Models\WarehouseGrade;
use App\Models\WarehouseOperator;
use App\Models\WarehouseOwner;
use Illuminate\Http\Request;

class WarehousesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouses = Warehouse::with(['operator', 'location', 'commodity', 'owner'])->latest()->paginate(10);
        return view('dashboard.warehouses.index', compact('warehouses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $locations = Location::latest()->get();
        $commodities = Commodity::latest()->get();
        $warehouses_owners = WarehouseOwner::latest()->get();
        $warehouse_operators = WarehouseOperator::latest()->get();

        return view('dashboard.warehouses.create', compact('warehouse_operators', 'locations', 'commodities', 'warehouses_owners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'operator' => 'required',
            'location' => 'required',
            'commodity_id' => 'required',
            'warehouse_owner_id' => 'required',
            'capacity' => 'required',
            'longitude' => 'required',
            'file_number' => 'required',
            'latitude' => 'required',
            'status' => 'required',
            ]);

        $warehouse = new Warehouse();
        $warehouse->name = $request->input('name');
        $warehouse->warehouse_operator_id = $request->input('operator');
        $warehouse->location_id = $request->input('location');
        $warehouse->commodity_id = $request->input('commodity_id');
        $warehouse->warehouse_owner_id = $request->input('warehouse_owner_id');
        $warehouse->capacity = $request->input('capacity');
        $warehouse->longitude = $request->input('longitude');
        $warehouse->latitude = $request->input('latitude');
        $warehouse->file_number = $request->input('file_number');
        $warehouse->status = $request->input('status');
        $warehouse->save();

        flash("Warehouse saved successful.")->success()->important();

        return redirect()->route('dashboard.warehouses.index');
    }
    
    public function storegrades(Request $request, Warehouse $warehouse)
    {

        $this->validate($request,[
            'grade' => 'required',
            'season'  => 'required',
        ]);

        $warehouse_grade = new WarehouseGrade();
        $warehouse_grade->warehouse_id = $warehouse->id;
        $warehouse_grade->grade_detail_id = $request->input('grade');
        $warehouse_grade->season_detail_id= $request->input('season');
        $warehouse_grade->status="1";
        $warehouse_grade->save();

        flash('successful')->success()->important();
        
        return redirect()->route('dashboard.warehouses.show', $warehouse->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Warehouse $warehouse)
    {
        $grades = GradeDetail::get();
        $seasons = SeasonDetail::get();
        return view('dashboard.warehouses.show',compact('grades','seasons','warehouse'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
