<?php

namespace App\Http\Controllers\Dashboard\PrimaryParties;

use App\Models\PrimaryParty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    public function index(PrimaryParty $primaryParty)
    {
    	return view('Dashboard.primary-parties.groups.index', compact('primaryParty'));
    }
}
