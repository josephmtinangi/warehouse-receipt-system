<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Commodity;
use App\Models\MainParty;
use Illuminate\Http\Request;

class MainPartiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mainParties = MainParty::latest()->get();
        return view('dashboard.main-parties.index', compact('mainParties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commodities = Commodity::get();
        return view('dashboard.main-parties.create', compact('commodities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'full_business_name' => 'required',
            'address' => 'required',
            'email' => 'required|unique:chama_kikuu',
            'phone_number' => 'required|unique:chama_kikuu',
            'commodity_id' => 'required',
        ]);

        $mainParty = new MainParty;
        $mainParty->full_business_name = $request->input('full_business_name');
        $mainParty->address = $request->input('full_business_name');
        $mainParty->email = $request->input('email');
        $mainParty->phone_number = $request->input('phone_number');
        $mainParty->commodity_id = $request->input('commodity_id');
        $mainParty->save();

        flash('Submitted successful')->success()->important();

        return redirect()->route('main-parties.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainParty = MainParty::with('primaryParties')->find($id);
        return view('dashboard.main-parties.show', compact('mainParty'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
