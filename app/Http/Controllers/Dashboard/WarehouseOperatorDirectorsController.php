<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\WarehouseOperator;
use App\Http\Controllers\Controller;
use App\Models\WarehouseOperatorDirector;

class WarehouseOperatorDirectorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $directors = WarehouseOperatorDirector::latest()->get();

        return view('dashboard.warehouse-operator-directors.index', compact('directors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouse_operators = WarehouseOperator::latest()->get();
        return view('dashboard.warehouse-operator-directors.create', compact('warehouse_operators'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'warehouse_operator_id' => 'required',
            'full_name' => 'required',
            'username' => 'required|unique:users',
            'phone_number' => 'required',
            'address' => 'required',
            'position' => 'required',
        ]);

        $role = Role::whereName('Warehouse')->firstOrFail();

        // 1. Create user
        $user = new User();
        $user->full_name = $request->input('full_name');
        $user->phone_number = $request->input('phone_number');
        $user->username = $request->input('username');
        $user->address = $request->input('address');
        $user->password = bcrypt($request->input('phone_number'));
        $user->role_id = $role->id;
        $user->status = true;
        $user->save();

        // 2. Create warehouse operator director
        $director = new WarehouseOperatorDirector();
        $director->warehouse_operator_id = $request->input('warehouse_operator_id');
        $director->full_name = $request->input('full_name');
        $director->position = $request->input('position');
        $director->status = $request->input('status');
        $director->user_id = $user->id;
        $director->save();

        flash("Warehouse operator director saved successful.")->success()->important();

        return redirect('dashboard/warehouse-operator-directors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
