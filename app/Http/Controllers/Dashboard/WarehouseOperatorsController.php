<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\License;
use App\Models\Location;
use App\Models\WarehouseOperator;
use Illuminate\Http\Request;

class WarehouseOperatorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouse_operators = WarehouseOperator::latest()->get();
        return view('Dashboard.warehouse-operators.index', compact('warehouse_operators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $licenses = License::latest()->get();
        $locations = Location::latest()->get();
        return view('Dashboard.warehouse-operators.create', compact('licenses', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'business_name' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'address' => 'required',
            'fax' => 'required',
            'location_id' => 'required',
            'license_id' => 'required',
        ]);

        $warehouse_operator = new WarehouseOperator;
        $warehouse_operator->business_name = $request->input('business_name');
        $warehouse_operator->phone_number = $request->input('phone_number');
        $warehouse_operator->email = $request->input('email');
        $warehouse_operator->address = $request->input('address');
        $warehouse_operator->fax = $request->input('fax');
        $warehouse_operator->location_id = $request->input('location_id');
        $warehouse_operator->license_id = $request->input('license_id');
        $warehouse_operator->save();

        flash("Warehouse operator saved successful.")->success()->important();

        return redirect('dashboard/warehouse-operators');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
