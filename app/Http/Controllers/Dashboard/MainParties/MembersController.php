<?php

namespace App\Http\Controllers\Dashboard\MainParties;

use App\Http\Controllers\Controller;
use App\Models\MainParty;
use App\Models\Position;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    public function create(MainParty $mainParty)
    {
        $positions = Position::get();
        return view('dashboard.main-parties.members.create', compact('mainParty', 'positions'));
    }

    public function store(Request $request, MainParty $mainParty)
    {
        $this->validate($request, [
            'cheo' => 'required',
            'full_name' => 'required',
            'phone_number' => 'required',
            'username' => 'required|unique:users',
            'address' => 'required',
        ]);

        $role = Role::firstOrCreate(['name' => 'Main Party']);

        $user = new User;
        $user->full_name = $request->input('full_name');
        $user->phone_number = $request->input('phone_number');
        $user->username = $request->input('username');
        $user->address = $request->input('address');
        $user->password = bcrypt($request->input('phone_number'));
        $user->role_id = $role->id;
        $user->status = 0;
        $user->save();

        $mainParty->members()->attach($user, ['cheo' => $request->input('cheo')]);

        flash('Member added successful.')->success()->important();

        return redirect()->route('main-parties.show', $mainParty->id);
    }
}
