<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Commodity;
use App\Models\Position;
use App\Models\PrimaryParty;
use Illuminate\Http\Request;

class PrimaryPartiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $primaryParties = PrimaryParty::with('commodity')->get();
        return view('dashboard.primary-parties.index', compact('primaryParties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commodities = Commodity::orderBy('name')->get();
        return view('dashboard.primary-parties.create', compact('commodities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'full_business_name' => 'required',
            'address' => 'required',
            'email' => 'required|email|unique:chama_cha_msingi',
            'phone_number' => 'required',
            'commodity_id' => 'required'
        ]);

        $primaryParty = new PrimaryParty;
        $primaryParty->full_business_name = $request->input('full_business_name');
        $primaryParty->address = $request->input('address');
        $primaryParty->email = $request->input('email');
        $primaryParty->phone_number = $request->input('phone_number');
        $primaryParty->commodity_id = $request->input('commodity_id');
        $primaryParty->save();

        flash()->success('Saved successful.')->important();

        return redirect()->route('primary-parties.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $positions = Position::get();
        $primaryParty = PrimaryParty::with('members')->findOrFail($id);
        return view('dashboard.primary-parties.show', compact('positions', 'primaryParty'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PrimaryParty $primaryParty)
    {
        $commodities = Commodity::orderBy('name')->get();
        return view('dashboard.primary-parties.edit', compact('primaryParty', 'commodities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrimaryParty $primaryParty)
    {
        $this->validate($request, [
            'full_business_name' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'commodity_id' => 'required'
        ]);

        $primaryParty->full_business_name = $request->input('full_business_name');
        $primaryParty->address = $request->input('address');
        $primaryParty->email = $request->input('email');
        $primaryParty->phone_number = $request->input('phone_number');
        $primaryParty->commodity_id = $request->input('commodity_id');
        $primaryParty->save();

        flash()->success('Updated successful!')->important();

        return redirect()->route('primary-parties.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
