<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Commodity;
use App\Models\District;
use App\Models\GradeDetail;
use App\Models\Item;
use App\Models\ItemOwnership;
use App\Models\License;
use App\Models\Loan;
use App\Models\Location;
use App\Models\MainParty;
use App\Models\Position;
use App\Models\PrimaryItem;
use App\Models\PrimaryParty;
use App\Models\PrimaryPartyGroup;
use App\Models\Region;
use App\Models\Role;
use App\Models\SeasonDetail;
use App\Models\User;
use App\Models\Warehouse;
use App\Models\WarehouseOperator;
use App\Models\WarehouseOperatorDirector;
use App\Models\WarehouseOwner;

class DashboardController extends Controller
{
    public function index()
    {
        $stats = [
            'banks' => Bank::count(),
            'commodities' => Commodity::count(),
            'districts' => District::count(),
            'gradeDetails' => GradeDetail::count(),
            'items' => Item::count(),
            'itemOwnerships' => ItemOwnership::count(),
            'licenses' => License::count(),
            'loans' => Loan::count(),
            'locations' => Location::count(),
            'mainParties' => MainParty::count(),
            'positions' => Position::count(),
            'primaryItems' => PrimaryItem::count(),
            'primaryParties' => PrimaryParty::count(),
            'primaryPartyGroups' => PrimaryPartyGroup::count(),
            'regions' => Region::count(),
            'roles' => Role::count(),
            'seasonDetails' => SeasonDetail::count(),
            'users' => User::count(),
            'warehouses' => Warehouse::count(),
            'warehouseOperators' => WarehouseOperator::count(),
            'warehouseOperatorDirectors' => WarehouseOperatorDirector::count(),
            'warehouseOwners' => WarehouseOwner::count(),

        ];

        return view('dashboard.dashboard.index', compact('stats'));
    }
}
