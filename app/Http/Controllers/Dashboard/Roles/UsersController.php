<?php

namespace App\Http\Controllers\Dashboard\Roles;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function create(Role $role)
    {
    	$users = User::get();
    	return view('dashboard.roles.users.create', compact('role', 'users'));
    }

    public function store(Request $request, Role $role)
    {
    	$this->validate($request, [
    		'user_id' => 'required',
    	]);

    	$user = User::findOrFail($request->input('user_id'));

    	$role->users()->save($user);

    	flash($user->full_name . ' assigned ' . $role->name . ' successful.')->success()->important();

    	return redirect()->route('roles.show', $role->id);
    }
}
