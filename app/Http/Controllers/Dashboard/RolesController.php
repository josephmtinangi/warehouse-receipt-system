<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    public function index()
    {
    	$roles = Role::withCount('users')->get();
    	return view('dashboard.roles.index', compact('roles'));
    }

    public function show(Role $role)
    {
    	return view('dashboard.roles.show', compact('role'));
    }

    public function edit(Role $role)
    {
    	return view('dashboard.roles.edit', compact('role'));
    }

    public function update(Request $request, Role $role)
    {

    	$role->update([
    		'description' => $request->description,
    	]);

    	flash('Role updated successful.')->success()->important();

    	return redirect()->route('roles.index');
    }
}
