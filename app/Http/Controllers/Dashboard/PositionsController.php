<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PositionsController extends Controller
{
    public function index()
    {
    	$positions = Position::latest()->get();
    	return view('dashboard.positions.index',compact('positions'));
    }
    public function create()
    {
     return view('dashboard.positions.create');
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
            'name' => 'required',
            'display_name' => 'required',
    		]);
        $position = new Position;
        $position->name = $request->input('name');
        $position->display_name = $request->input('display_name');
        $position->description = $request->input('description');
        $position->save();

        flash('Position saved successful')->success()->important();

        return redirect()->route('positions.index');

    }
}
