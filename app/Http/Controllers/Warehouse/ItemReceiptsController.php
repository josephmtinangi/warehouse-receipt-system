<?php

namespace App\Http\Controllers\Warehouse;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Warehouse;

class ItemReceiptsController extends Controller
{
    public function create(Warehouse $warehouse, Item $item)
    {
        return view('warehouse.warehouses.items.receipts.create', compact('warehouse', 'item'));
    }
}
