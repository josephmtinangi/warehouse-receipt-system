<?php

namespace App\Http\Controllers\Warehouse\PrimaryParty\Groups;

use App\Models\Item;
use App\Models\Commodity;
use App\Models\Warehouse;
use App\Models\PrimaryParty;
use Illuminate\Http\Request;
use App\Models\ItemOwnership;
use App\Models\PrimaryPartyGroup;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    public function create(Warehouse $warehouse, PrimaryParty $primaryParty, $groupId)
    {
    	$commodities = Commodity::get();
    	$primaryPartyGroup = PrimaryPartyGroup::with('secondaryItems')->findOrFail($groupId);
    	return view('warehouse.warehouses.primary-parties.groups.items.create', compact('commodities', 'warehouse', 'primaryParty', 'primaryPartyGroup'));
    }

    public function store(Request $request, Warehouse $warehouse, PrimaryParty $primaryParty, $groupId)
    {
    	$primaryPartyGroup = PrimaryPartyGroup::with('secondaryItems')->findOrFail($groupId);

    	$this->validate($request, [
    		'commodity_id' 		=> 'required',
    		'commodity_origin' 	=> 'required',
    		'quantity' 			=> 'required|numeric',
    		'weight' 			=> 'required|numeric',
    		'grade' 			=> 'required',
    		'class' 			=> 'required',
    		'moisture' 			=> 'required',
    	]);

    	$item = new Item;
    	$item->commodity_id 		= $request->input('commodity_id');
    	$item->commodity_origin 	= $request->input('commodity_origin');
    	$item->quantity 			= $request->input('quantity');
    	$item->weight 				= $request->input('weight');
    	$item->grade 				= $request->input('grade');
    	$item->grade 				= $request->input('grade');
    	$item->class 				= $request->input('class');
    	$item->moisture 			= $request->input('moisture');
    	$item->property_one 		= $request->input('property_one');
    	$item->property_two 		= $request->input('property_two');
    	$item->property_three 		= $request->input('property_three');
    	$item->property_four 		= $request->input('property_four');
    	$item->group_id			 	= $primaryPartyGroup->id;
    	$item->depositor_user_id 	= $primaryPartyGroup->members()->first()->id;
    	$item->receiver_user_id 	= auth()->id();
    	
    	$warehouse->items()->save($item);

        $itemOwnership = new ItemOwnership;
        $itemOwnership->item_id = $item->id;
        $itemOwnership->status = 'Active';
        $itemOwnership->owner_group_id = $primaryPartyGroup->id;
        $itemOwnership->owner_flag = 'Group';
        $itemOwnership->created_by = auth()->id();
        $itemOwnership->save();        

    	flash('Item added successful.')->success(); 

    	return redirect()->route('warehouses.primary-parties.primary-party.groups.show', [$warehouse->id, $primaryParty->id, $primaryPartyGroup->id]);
    }
}
