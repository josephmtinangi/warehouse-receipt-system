<?php

namespace App\Http\Controllers\Warehouse\PrimaryParty;

use App\Models\Warehouse;
use App\Models\PrimaryParty;
use Illuminate\Http\Request;
use App\Models\PrimaryPartyGroup;
use App\Http\Controllers\Controller;

class PrimaryPartyGroupsController extends Controller
{
    public function index(Warehouse $warehouse, PrimaryParty $primaryParty)
    {
    	return view('warehouse.warehouses.primary-parties.groups.index', compact('warehouse', 'primaryParty'));
    }

    public function show(Warehouse $warehouse, PrimaryParty $primaryParty, $groupId)
    {
    	$primaryPartyGroup = PrimaryPartyGroup::findOrFail($groupId);
    	return view('warehouse.warehouses.primary-parties.groups.show', compact('warehouse', 'primaryParty', 'primaryPartyGroup'));	
    }
}
