<?php

namespace App\Http\Controllers\Warehouse;

use App\Models\Role;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FarmersController extends Controller
{
    public function index($warehouseId)
    {
        $warehouse = Warehouse::with(['farmers' => function ($query) {
            $query->orderBy('created_at', 'DESC');
        }])->findOrFail($warehouseId);
    	return view('warehouse.warehouses.farmers.index', compact('warehouse'));
    }

    public function show(Warehouse $warehouse, $farmerId)
    {
        $farmer = $warehouse->farmers()->findOrFail($farmerId);
        return view('warehouse.warehouses.farmers.show', compact('warehouse', 'farmer'));
    }

    public function create(Warehouse $warehouse)
    {
    	return view('warehouse.warehouses.farmers.create', compact('warehouse'));	
    }

    public function store(Request $request, Warehouse $warehouse)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'phone_number' => 'required|unique:users',
            'address' => 'required',
        ]);

        $role = Role::firstOrCreate(['name' => 'Farmer']);

        $user = new User();
        $user->full_name = $request->input('full_name');
        $user->phone_number = $request->input('phone_number');
        $user->username = $request->input('phone_number');
        $user->address = $request->input('address');
        $user->password = bcrypt($request->input('phone_number'));
        $user->role_id = $role->id;
        $user->status = 0;
        $user->save();

        $warehouse->farmers()->attach([$user->id]);

        flash('Farmer added successful.')->success()->important();

        return redirect()->route('warehouses.farmers.index', $warehouse->id);
    }
}
