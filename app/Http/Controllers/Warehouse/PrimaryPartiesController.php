<?php

namespace App\Http\Controllers\Warehouse;

use App\Models\Warehouse;
use App\Models\PrimaryParty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrimaryPartiesController extends Controller
{
    public function index(Warehouse $warehouse)
    {
    	return view('warehouse.warehouses.primary-parties.index', compact('warehouse'));
    }

    public function show(Warehouse $warehouse, PrimaryParty $primaryParty)
    {
    	return view('warehouse.warehouses.primary-parties.show', compact('warehouse', 'primaryParty'));
    }
}
