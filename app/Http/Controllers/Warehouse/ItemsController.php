<?php

namespace App\Http\Controllers\Warehouse;

use App\Models\Item;
use App\Models\Warehouse;
use App\Models\Commodity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    public function index($warehouseId)
    {
        $warehouse = Warehouse::with('items')->findOrFail($warehouseId);
    	return view('warehouse.warehouses.items.index', compact('warehouse'));
    }

    public function show(Warehouse $warehouse, Item $item)
    {
    	$farmers = $warehouse->farmers;
    	
    	$groups = [];
    	foreach ($warehouse->primaryParties as $primaryParty) {
    		$groups[] = $primaryParty->groups;
    	}

    	$groups = $groups[0];

    	return view('warehouse.warehouses.items.show', compact('warehouse', 'item', 'farmers', 'groups'));
    }
}
