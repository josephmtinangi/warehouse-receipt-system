<?php

namespace App\Http\Controllers\Warehouse;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WarehousesController extends Controller
{
	public function index()
	{
		$warehouses = auth()->user()->director->operator->warehouses;
		return view('warehouse.warehouses.index', compact('warehouses'));
	}
	public function show($id)
	{
	 	$warehouse = auth()->user()->director->operator->warehouses()->with(['items', 'farmers', 'primaryParties'])->findOrFail($id);
	 	return view('warehouse.warehouses.show', compact('warehouse'));
	}
	public function viewdetails($id)
	{
	$warehouse = auth()->user()->director->operator->warehouses()->with(['items', 'farmers', 'primaryParties'])->findOrFail($id);
	 	return view('warehouse.warehouses.details', compact('warehouse'));	
	}
}
