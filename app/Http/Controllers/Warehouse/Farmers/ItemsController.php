<?php

namespace App\Http\Controllers\Warehouse\Farmers;

use App\Models\Item;
use App\Models\Warehouse;
use App\Models\Commodity;
use Illuminate\Http\Request;
use App\Models\ItemOwnership;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    public function create(Warehouse $warehouse, $farmerId)
    {
    	$commodities = Commodity::get();
    	$farmer = $warehouse->farmers()->findOrFail($farmerId);
    	return view('warehouse.warehouses.farmers.items.create', compact('commodities', 'warehouse', 'farmer'));
    }

    public function store(Request $request, Warehouse $warehouse, $farmerId)
    {
    	$farmer = $warehouse->farmers()->findOrFail($farmerId);

    	$this->validate($request, [
    		'commodity_id' 		=> 'required',
    		'commodity_origin' 	=> 'required',
    		'quantity' 			=> 'required|numeric',
    		'weight' 			=> 'required|numeric',
    		'grade' 			=> 'required',
    		'class' 			=> 'required',
    		'moisture' 			=> 'required',
    	]);

    	$item = new Item;
    	$item->commodity_id 		= $request->input('commodity_id');
    	$item->commodity_origin 	= $request->input('commodity_origin');
    	$item->quantity 			= $request->input('quantity');
    	$item->weight 				= $request->input('weight');
    	$item->grade 				= $request->input('grade');
    	$item->grade 				= $request->input('grade');
    	$item->class 				= $request->input('class');
    	$item->moisture 			= $request->input('moisture');
    	$item->property_one 		= $request->input('property_one');
    	$item->property_two 		= $request->input('property_two');
    	$item->property_three 		= $request->input('property_three');
    	$item->property_four 		= $request->input('property_four');
    	$item->depositor_user_id 	= $farmer->id;
    	$item->receiver_user_id 	= auth()->id();
    	

        // Assign that item to warehouse
    	$warehouse->items()->save($item);

        // Create ownership history
        $itemOwnership = new ItemOwnership;
        $itemOwnership->item_id = $item->id;
        $itemOwnership->status = 'Active';
        $itemOwnership->owner_user_id = $farmer->id;
        $itemOwnership->owner_flag = 'Individual';
        $itemOwnership->created_by = auth()->id();
        $itemOwnership->save();        

    	flash('Item added successful.')->success();

    	return redirect()->route('warehouses.farmers.show', [$warehouse->id, $farmer->id]);
    }
}
