<?php

namespace App\Http\Controllers\Warehouse;

use App\Models\Item;
use App\Models\User;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use App\Models\ItemOwnership;
use App\Models\PrimaryPartyGroup;
use App\Http\Controllers\Controller;

class TransferController extends Controller
{
    public function individual(Request $request, Warehouse $warehouse, Item $item)
    {
    	$this->validate($request, [
    		'owner_user_id' => 'required',
    	]);

    	$user = User::findOrFail($request->input('owner_user_id'));

    	// Fetch old ownership
    	$old_ownership = $item->ownerships()->whereStatus('Active')->first();
    	$old_ownership->status = 'Inactive';
    	$old_ownership->save();

    	$itemOwnership = new ItemOwnership;
    	$itemOwnership->item_id = $item->id;
    	$itemOwnership->status = 'Active';
    	$itemOwnership->owner_user_id = $user->id;
    	$itemOwnership->owner_flag = 'Individual';
    	$itemOwnership->created_by = auth()->id();
    	$itemOwnership->save();

    	flash('Transfer success.')->success();

    	return back();
    }

    public function group(Request $request, Warehouse $warehouse, Item $item)
    {
    	$this->validate($request, [
    		'owner_group_id' => 'required',
    	]);

    	$group = PrimaryPartyGroup::findOrFail($request->input('owner_group_id'));

    	// Fetch old ownership
    	$old_ownership = $item->ownerships()->whereStatus('Active')->first();
    	$old_ownership->status = 'Inactive';
    	$old_ownership->save();

    	$itemOwnership = new ItemOwnership;
    	$itemOwnership->item_id = $item->id;
    	$itemOwnership->status = 'Active';
    	$itemOwnership->owner_group_id = $group->id;
    	$itemOwnership->owner_flag = 'Group';
    	$itemOwnership->created_by = auth()->id();
    	$itemOwnership->save();

    	flash('Transfer success.')->success();

    	return back();    	
    }
}
