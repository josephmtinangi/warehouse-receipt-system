<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $chairperson = \App\Models\Position::whereName('chairperson')->first();

        // foreach(auth()->user()->primaryParties as $primaryParty){
        //     foreach($primaryParty->members as $member){
        //         if($member->pivot->user_id !== auth()->id() && $member->pivot->cheo !== $chairperson->id) {
        //             return 'forbidden';
        //         }
        //     }
        // }

        // return 'authorized';
        return view('home');
    }
}
