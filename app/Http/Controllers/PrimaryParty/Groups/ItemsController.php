<?php

namespace App\Http\Controllers\PrimaryParty\Groups;

use App\Models\Commodity;
use App\Models\PrimaryItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($primaryPartyId, $groupId)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $group = $primaryParty->groups()->with(['items' => function ($query) {
            $query->orderBy('created_at', 'DESC');
        }])->findOrFail($groupId);
        return view('primary-party.primary-party.groups.items.index', compact('primaryParty', 'group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($primaryPartyId, $groupId)
    {
        $commodities = Commodity::get();
        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $group = $primaryParty->groups()->with('members')->findOrFail($groupId);
        return view('primary-party.primary-party.groups.items.create', compact('commodities', 'primaryParty', 'group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $primaryPartyId, $groupId)
    {

        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $group = $primaryParty->groups()->findOrFail($groupId);

        $this->validate($request, [
            'commodity_id' => 'required',
            'quantity' => 'required',
            'weight' => 'required',
            'grade' => 'required',
            'class' => 'required',
        ]);

        $primaryItem = new PrimaryItem;
        $primaryItem->commodity_id = $request->input('commodity_id');
        $primaryItem->quantity = $request->input('quantity');
        $primaryItem->weight = $request->input('weight');
        $primaryItem->grade = $request->input('grade');
        $primaryItem->class = $request->input('class');
        $primaryItem->depositor_user_id = auth()->id();
        $primaryItem->receiver_user_id = auth()->id();
        $primaryItem->class = $request->input('class');
        $primaryItem->save();
        
        $group->items()->attach([$primaryItem->id]);

        flash('Item added successful.')->success()->important();

        return redirect()->route('primary-party.primary-party.groups.items.index', [$primaryParty->id, $group->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
