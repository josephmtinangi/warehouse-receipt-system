<?php

namespace App\Http\Controllers\PrimaryParty\Groups;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    public function index($primaryPartyId, $groupId)
    {
    	$primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $group = $primaryParty->groups()->with('members')->findOrFail($groupId);
    	return view('primary-party.primary-party.groups.members.index', compact('primaryParty', 'group'));
    }

    public function create($primaryPartyId, $groupId)
    {
    	$primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $group = $primaryParty->groups()->with('members')->findOrFail($groupId);

        $members = $primaryParty->members;

        return view('primary-party.primary-party.groups.members.create', compact('primaryParty', 'group', 'members'));
    }

    public function store(Request $request, $primaryPartyId, $groupId)
    {
    	$this->validate($request, [
    		'user_id' => 'required',
    	]);

    	$primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $group = $primaryParty->groups()->with('members')->findOrFail($groupId);

        $user = User::findOrFail($request->input('user_id'));
        $user = $primaryParty->members()->findOrFail($user->id);

        $group->members()->attach([$user->id]);

        flash('Member added successful.')->success()->important();

        return redirect()->route('primary-party.primary-party.groups.members', [$primaryParty->id, $group->id]);
    }
}
