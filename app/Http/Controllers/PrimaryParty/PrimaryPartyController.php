<?php

namespace App\Http\Controllers\PrimaryParty;

use App\Http\Controllers\Controller;
use App\Models\Position;
use App\Models\PrimaryParty;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class PrimaryPartyController extends Controller
{
    public function index()
    {
        $primaryParties = PrimaryParty::latest()->get();
        return view('primary-party.primary-party.index', compact('primaryParties'));
    }

    public function show($id)
    {
        $primaryParty = auth()->user()->primaryParties()->with(['members', 'groups'])->findOrFail($id);
        return view('primary-party.primary-party.show', compact('primaryParty'));
    }

    public function createMember($id)
    {
        $farmers = Role::whereName('Farmer')->first()->users;
        $positions = Position::get();
        $primaryParty = auth()->user()->primaryParties()->with('members')->findOrFail($id);
        return view('primary-party.primary-party.members.create', compact('farmers', 'positions', 'primaryParty'));
    }

    public function storeMember(Request $request, $id)
    {
        // fetch primary party
        $primaryParty = auth()->user()->primaryParties()->with('members')->findOrFail($id);

        // validate
        $this->validate($request, [
            'cheo' => 'required',
            'full_name' => 'required',
            'phone_number' => 'required',
            'username' => 'required|unique:users',
            'address' => 'required',
        ]);

        // get farmer role
        $role = Role::whereName('Farmer')->firstOrFail();

        // create user
        $user = new User;
        $user->full_name = $request->input('full_name');
        $user->phone_number = $request->input('phone_number');
        $user->username = $request->input('username');
        $user->address = $request->input('address');
        $user->role_id = $role->id;
        $user->password = bcrypt($request->input('phone_number'));
        $user->save();

        // add that user as a member
        $primaryParty->members()->attach($user, ['cheo' => $request->input('cheo')]);

        flash('Member registered successful')->important();

        return redirect()->route('primary-party.primary-party.members.index', $primaryParty);
    }

    public function storeExistingMember(Request $request, $id)
    {
        // fetch primary party
        $primaryParty = auth()->user()->primaryParties()->with('members')->findOrFail($id);

        // validate
        $this->validate($request, [
            'user_id' => 'required',
            'cheo' => 'required',
        ]);

        $user = User::findOrFail($request->input('user_id'));

        // get farmer role
        $role = Role::whereName('Farmer')->firstOrFail();

        // add that user as a member
        $primaryParty->members()->attach($user, ['cheo' => $request->input('cheo')]);

        flash('Member registered successful')->important();

        return redirect()->route('primary-party.primary-party.members.index', $primaryParty);        
    }
}
