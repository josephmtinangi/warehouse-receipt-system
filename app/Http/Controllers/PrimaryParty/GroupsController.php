<?php

namespace App\Http\Controllers\PrimaryParty;

use Illuminate\Http\Request;
use App\Models\PrimaryPartyGroup;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($id);
        $groups = $primaryParty->groups()->paginate(20);
        return view('primary-party.primary-party.groups.index', compact('primaryParty', 'groups'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($id);
        return view('primary-party.primary-party.groups.create', compact('primaryParty'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $primaryParty = auth()->user()->primaryParties()->findOrFail($id);

        $group = new PrimaryPartyGroup;
        $group->name = $request->input('name');
        $group->description = $request->input('description');
        $group->code = str_random(6);
        $group->chama_cha_msingi_id = $primaryParty->id;
        $group->save();

        $primaryParty->groups()->attach($group);
        
        flash('Group created successful')->success()->important();


        return redirect()->route('primary-party.primary-party.groups.index', $primaryParty);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($primaryPartyId, $groupId)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $group = $primaryParty->groups()->with(['members', 'items'])->findOrFail($groupId);
        return view('primary-party.primary-party.groups.show', compact('primaryParty', 'group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
