<?php

namespace App\Http\Controllers\PrimaryParty\Members;

use App\Models\Commodity;
use App\Models\PrimaryItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($primaryPartyId, $userId)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $member = $primaryParty->members()->with('items')->findOrFail($userId);        
        return view('primary-party.primary-party.members.items.index', compact('primaryParty', 'member'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($primaryPartyId, $userId)
    {
        $commodities = Commodity::orderBy('name')->get();
        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $member = $primaryParty->members()->with('items')->findOrFail($userId);        
        return view('primary-party.primary-party.members.items.create', compact('commodities', 'primaryParty', 'member'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $primaryPartyId, $userId)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $user = $primaryParty->members()->with('items')->findOrFail($userId);

        $this->validate($request, [
            'commodity_id' => 'required',
            'quantity' => 'required',
            'weight' => 'required',
            'grade' => 'required',
            'class' => 'required',
        ]);

        $primaryItem = new PrimaryItem;
        $primaryItem->commodity_id = $request->input('commodity_id');
        $primaryItem->quantity = $request->input('quantity');
        $primaryItem->weight = $request->input('weight');
        $primaryItem->grade = $request->input('grade');
        $primaryItem->class = $request->input('class');
        $primaryItem->depositor_user_id = $user->id;
        $primaryItem->receiver_user_id = auth()->id();
        $primaryItem->class = $request->input('class');
        $primaryItem->save();

        flash('Item added successful.')->success()->important();

        return redirect()->route('primary-party.primary-party.members.items.index', [$primaryParty->id, $user->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
