<?php

namespace App\Http\Controllers\PrimaryParty;

use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    public function index($id)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($id);
        $members = $primaryParty->members()->latest()->paginate(20);
        return view('primary-party.primary-party.members.index', compact('primaryParty', 'members'));
    }

    public function show($primaryPartyId, $userId)
    {
        $primaryParty = auth()->user()->primaryParties()->findOrFail($primaryPartyId);
        $member = $primaryParty->members()->with('items')->findOrFail($userId);
        return view('primary-party.primary-party.members.show', compact('primaryParty', 'member'));
    }
}
