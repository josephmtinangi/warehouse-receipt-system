<?php

namespace App\Http\Controllers\MainParty;

use App\Models\MainParty;
use App\Http\Controllers\Controller;

class MainPartyController extends Controller
{
    public function show($id)
    {
        $mainParty = auth()->user()->mainParties()->findOrFail($id);
        return view('main-party.main-party.show', compact('mainParty'));
    }
}
