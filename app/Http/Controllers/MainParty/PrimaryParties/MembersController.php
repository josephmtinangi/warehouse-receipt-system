<?php

namespace App\Http\Controllers\MainParty\PrimaryParties;

use App\Http\Controllers\Controller;
use App\Models\MainParty;
use App\Models\Position;
use App\Models\PrimaryParty;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    public function index(MainParty $mainParty)
    {
        return $mainParty;
    }

    public function create(MainParty $mainParty, PrimaryParty $primaryParty)
    {
        $positions = Position::get();
        return view('main-party.main-party.primary-parties.members.create', compact('mainParty', 'primaryParty', 'positions'));
    }

    public function store(Request $request, MainParty $mainParty, PrimaryParty $primaryParty)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'phone_number' => 'required|unique:users',
            'address' => 'required',
        ]);

        $role = Role::firstOrCreate(['name' => 'Farmer']);

        $user = new User();
        $user->full_name = $request->input('full_name');
        $user->phone_number = $request->input('phone_number');
        $user->username = $request->input('phone_number');
        $user->address = $request->input('address');
        $user->password = bcrypt($request->input('phone_number'));
        $user->role_id = $role->id;
        $user->status = 0;
        $user->save();

        $primaryParty->members()->attach($user, ['cheo' => $request->input('cheo')]);

        flash('Member added successful.')->success()->important();

        return redirect()->route('main-party.main-party.primary-parties.show', [$mainParty->id, $primaryParty->id]);
    }
}
