<?php

namespace App\Http\Controllers\MainParty;

use App\Models\MainParty;
use App\Models\Commodity;
use App\Models\PrimaryParty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrimaryPartyController extends Controller
{
    public function index(MainParty $mainParty)
    {
    	return view('main-party.main-party.primary-parties.index', compact('mainParty'));
    }

    public function create(MainParty $mainParty)
    {
    	$commodities = Commodity::get();
    	return view('main-party.main-party.primary-parties.create', compact('mainParty', 'commodities'));	
    }

    public function store(Request $request, MainParty $mainParty)
    {
        $this->validate($request, [
            'full_business_name' => 'required',
            'address' => 'required',
            'email' => 'required|email|unique:chama_cha_msingi',
            'phone_number' => 'required',
            'commodity_id' => 'required'
        ]);

        $primaryParty = new PrimaryParty;
        $primaryParty->full_business_name = $request->input('full_business_name');
        $primaryParty->address = $request->input('address');
        $primaryParty->email = $request->input('email');
        $primaryParty->phone_number = $request->input('phone_number');
        $primaryParty->commodity_id = $request->input('commodity_id');
        $primaryParty->save();

        $mainParty->primaryParties()->attach($primaryParty);

        flash()->success('Saved successful.')->important();

        return redirect()->route('main-party.main-party.primary-parties.index', $mainParty->id);    	
    }

    public function show(MainParty $mainParty, PrimaryParty $primaryParty)
    {
    	return view('main-party.main-party.primary-parties.show', compact('mainParty', 'primaryParty'));
    }
}
